#!/usr/bin/env python
#-*- coding: utf-8 -*-
#=========================
# Filename:  lamp_core_base.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-08-08
#-------------------------
# Description:
#
#-------------------------
# License:
#
#=========================
from lamp_utils.lamp_exceptions import *
import lamp_hardware_manager
from lamp_application_manager import *
import lamp_connection_tools.lamp_bluetooth_manager

class LampCoreBase(object):
    def __init__(self):
        pass
    
