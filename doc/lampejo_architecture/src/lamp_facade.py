#!/usr/bin/env python
#-*- coding: utf-8 -*-
#=========================
# Filename: lamp_facade.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-08-08
#-------------------------
# Description:
#
#-------------------------
# License:
#
#=========================
import lamp_core_base

class LampFacade(object):
    def do_action():
        pass
        
    def setup_interface():
        pass
        
    def __init__(self):
        pass
    
