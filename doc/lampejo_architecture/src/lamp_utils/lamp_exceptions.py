#!/usr/bin/env python
#-*- coding: utf-8 -*-
#=========================
# Filename:  lamp_exceptions.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-08-08
#-------------------------
# Description:
#
#-------------------------
# License:
#
#=========================

class Exception(object):
    def __init__(self):
        pass
    
class TesteError(Exception):
    def __init__(self):
        pass
    
