#!/usr/bin/env python
#-*- coding: utf-8 -*-
#=========================
# Filename:  lamp_bluetooth_manager.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-08-08
#-------------------------
# Description:
#
#-------------------------
# License:
#
#=========================

class LampBluetoothManager(object):
    def send_data():
        """Send to wireless comunication the data_send_buffer contents."""
        pass
        
    def receive_data():
        """It stores the data received from wireless communication into the data_recv_buffer"""
        pass
        
    def pack_data():
        """Build a packet with the data and put it in the data_send_buffer"""
        pass
        
    def unpack_data():
        """Return a string with the raw content, without packet data. The data argument must be a data packet."""
        pass
        
    def __init__(self):
        self.__data_recv_buffer = ""
        self.__data_send_buffer = ""
        pass
    
