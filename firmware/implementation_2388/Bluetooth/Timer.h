
#ifndef __TIMERDEF__
#define __TIMERDEF__

//#define	BYTE	unsigned char

#define MATCH_VALUE_1MS	 	(PCLK * TIME_BASE)/((1 + PREESCALER_VALUE )* 1000)
#define MATCH_VALUE_5uS	 	(PCLK * TIME_BASE)/((1 + PREESCALER_VALUE )* 200000)
#define TIME_RESET				2000
#define TIME_OUT_VALUE 		200
#define CONFIG_RX_IO 			0xFFF3FFFF	
#define TIME_OUT_BIT 			15
#define FOUR_SECOND 			4000

#define DISABLE_COUNTER		0x00
#define ENABLE_COUNTER		0x01
#define CLEAR_COUNTER		0x00
#define PREESCALER_VALUE	0x01
#define ENABLE_INTERRUPT	0x01
#define DISABLE_INTERRUPT	0x00
#define RESET_COUNTER		0x02
#define TIME_BASE			0x01
#define PCLK				12000000
#define TIMER_0_Priority	0x07
#define	INT_TIMER0_ENABLE  	0x10
#define	INT_TIMER1_ENABLE  	0x20
// int TIME = 0;
//extern int i;
void Delay(int temp);
BOOL InitTimer(void);
void LigaRelogio(void);
void fSW_Turn_on_Timer(BYTE bTimer);
void DesligaRelogio(void);
void Timer0_Interrupt(void)__irq;
void Timer1_Interrupt(void)__irq;
void set_TIME(int value,int timer);
int get_TIME(int timer);

#endif
