#include "lpc23xx.h"
#include "type.h"
#include "timer.h"
#include "ad.h"
//#include "interrupt.h"
#include "irq.h"

short s_buffer[5120];
int counter =0;
int CATCH_BLOCK =0; 

BOOL InitAd(void){
	BOOL ret = FALSE;
    PCONP |= ENABLE_ADC;
    //PINSEL10 &= 0x00000000;
	PINSEL1 |= AD0_2;
	AD0INTEN |= 0x100;//000001;
	AD0CR = CLK_DIV_AD_6 + PDN_OPERATIONAL + SEL_AD0_2; 
	 //if(install_irq(ADC0_INT, (void *) AD_Interrupt, ADC_0_Priority )){
      // ret = TRUE;
     //}else {
	  // ret = FALSE;
    //} 
    
	//AD0CR |= START_CONVERSION;

 	//Delay(100);
	//AD0CR |= START_CONVERSION;
	//AD0INTEN |= 0x100;//0x00000001;

	return ret;
}
void StopAd(void){

    AD0CR &= (~PDN_OPERATIONAL); 
    AD0CR |= STOP_CONVERSION;
	
}

int ReadAd(void){
	
	int val;
	//Delay(2);
	
	AD0CR |= START_CONVERSION;
	while ((AD0GDR & DONE_CAPTURE) == 0);	//	espera fim de captura
	val = ((AD0DR2 >> 6) & MAX_VALUE_AD);

	return(val);

}

int ConverteAdPixel(void){

	int valor;
	int pixel;
	
	valor = ReadAd();
	pixel =  (valor * 100)/MAX_VALUE_AD;
	
	return(pixel);

}

void AD_Interrupt(void)__irq{
  
	
	DACR = AD0DR2;//ReadAd()<<6;
	/*
	s_buffer[counter] = ((AD0DR2 >> 6) & MAX_VALUE_AD); 
	counter++;
	if(counter==5120){
	    CATCH_BLOCK = 1;
		StopAd();
	} */
    
	VICVectAddr = 0;
}

