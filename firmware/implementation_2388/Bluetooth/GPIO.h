//#define ENABLE_CAM() (FIO4SET |= 0x01)
//#define DISABLE_CAM() (FIO4CLR |= 0x01)
//#define RESET_LOW_CAM() (FIO4CLR |= 0x02)  
//#define RESET_HIGH_CAM() (FIO4SET |= 0x02)
//#define RESET_LOW_BLUETOOTH() (FIO4CLR |= 0x04) //Mudar Pino
//#define RESET_HIGH_BLUETOOTH() (FIO4SET |= 0x04) //Mudar Pino
#define MOVE_UP()  (!(IOPIN1 & (1<<18))) 
#define MOVE_DOWN() (!(IOPIN1 & (1<<19)))
#define MOVE_LEFT()  (!(IOPIN1 & (1<<27)))
#define MOVE_RIGHT()  (!(IOPIN1 & (1<<22)))
#define ENTER_BUTTON()    (!(IOPIN1 & (1<<25)))
#define BUTTON1()    (!(FIO2PIN & 1))
#define BUTTON2()    (!(FIO2PIN & (1<<1)))
#define BUTTON3()    (!(FIO2PIN & (1<<2)))
#define BUTTON4()    (!(FIO2PIN & (1<<3)))
#define BUTTON5()    (!(FIO2PIN & (1<<4)))
#define BUTTON6()    (!(FIO2PIN & (1<<5)))
#define ALT_TAB_BUTTON() (!(IOPIN0 & (1<<18))) //(!(FIO2PIN & (1<<6)))  
#define HOME_BUTTON() (!(IOPIN0 & (1<<29)))//(!(IOPIN0 & (1<<31)))

void GPIO_Init(void);  


  