#include <stdio.h>
#include <LPC23xx.H>                    /* LPC23xx definitions                */
#include "type.h"
#include "irq.h"
#include "ApplicationProtocol.h" 
#include "uart.h"
#include "SerialBluetooth.h"
#include "timer.h"
#include "GPIO.h"
#include "lcd.h"
#include "telas.h"
#include "MainApplication.h"
#include "mci.h"
#include "ad.h"
#include "SD.h"




char stateBut_Jstk =0;
short state_buttons =0;
short Button_Pressed =0;
char debounce =0;


void Send_Jstk_Command(short n_button){

   switch(n_button){
	   case UP_BUTTON:
	       Send_Move(U);
	   break;
	   case DOWN_BUTTON:
		   Send_Move(D);
	   break;
	   case LEFT_BUTTON:
		   Send_Move(L);
	   break;
	   case RIGHT_BUTTON:
		   Send_Move(R);
	   break;
  }
}						   
void Send_Buttons_Commands(char n_button){
	switch(n_button){
	   case BUTTON_1:
	       Send_Button('1');   
	   break;
	   case BUTTON_2:
	       Send_Button('2');
	   break;
	   case BUTTON_3:
	       Send_Button('3'); 
	   break;
	   case BUTTON_4:
	       Send_Button('4');	   
	   break;
	   case BUTTON_5:
		   Send_Button('5');
	   break;
	   case BUTTON_6:
		   Send_Button('6');
	   break;
	   case ALT_BUTTON:
	       Send_Button('A');	   
	   break;
	   case HOME_BUTTON_:
		   Send_Button('H');
	   break;	  
	   case ENTER:
		   Send_ENTER();
	   break;	  
   
   }
}
void JoystickCommands(char Button,char n_button){
	char state = ((stateBut_Jstk>>n_button)&(0x01));
	switch(state){
        case NOT_PRESSED:
		    if(Button){
			   Delay(10);
			   if(Button){
			      Send_Jstk_Command(n_button);
			      LigaRelogio();
			      set_TIME(500,n_button);
			      stateBut_Jstk |= (PRESSED<<n_button);
			   }
			}       
		break;
		case PRESSED:
		    if(Button){
			   if(get_TIME(n_button)==0){
			      Send_Jstk_Command(n_button);
				  set_TIME(500,n_button);   
			   }
			}else {
			     Delay(10); 
			     if(!Button){
				     stateBut_Jstk &= (~(PRESSED<<n_button));
				     DesligaRelogio();
				 } 			
			}    
		break;

		
    }
} 





void Buttons(char Button,char n_button) {
  char state = ((state_buttons>>n_button)&(0x01));
  //Button_Pressed =0;
  switch(state){   
      
   case NOT_PRESSED:
   		if(Button){
		  Delay(10);
	      if(Button){
			   Button_Pressed = n_button+1;
			   Send_Buttons_Commands(n_button);
			   //Delay(10);
			   state_buttons = (PRESSED<<n_button);
		  }
		}
				    
   break;
         
   case PRESSED:
   		
		    if(Button){
			  state_buttons = (PRESSED<<n_button);  
			}else{
			  Delay(10);
			  if(!Button){
		      state_buttons &= (~(PRESSED<<n_button));
			  }
		    }    
	break;
 }
}

void Change_Screen(int code) {
	int img_code = code;
    switch(img_code) {

	   case TELA_HOME:
	   TelaHome();
	   break;
       
	   case MENU_ARQ://MENU_FILES_CODE:
	   MenuArquivos();
	   break;
	
	   case RECENTES://SCREEN_PICTURES_CODE:
	   Recentes();  
	   break;
	   
	   case TELA_FIGURAS:
	   TelaFiguras();
	   break;
  
       case TELA_GENERICA:
	   TelaGenerica();
	   break;
	   default:
	   TelaGenerica(); 
	   break;	
	}
}

void Comunication(void) {

 		JoystickCommands(MOVE_UP(),UP_BUTTON);
		JoystickCommands(MOVE_DOWN(),DOWN_BUTTON);
		JoystickCommands(MOVE_LEFT(),LEFT_BUTTON);
		JoystickCommands(MOVE_RIGHT(),RIGHT_BUTTON);
		Buttons(ENTER_BUTTON(),ENTER);
		Buttons(BUTTON1(),BUTTON_1);
		Buttons(BUTTON2(),BUTTON_2);
		Buttons(BUTTON3(),BUTTON_3);
		Buttons(BUTTON4(),BUTTON_4);
		Buttons(BUTTON5(),BUTTON_5);
		Buttons(BUTTON6(),BUTTON_6);
		Buttons(ALT_TAB_BUTTON(),ALT_BUTTON);
		Buttons(HOME_BUTTON(),HOME_BUTTON_);
		if(PACKET_RECEIVED){
	         Receive_MSG();
        }
		 
}

void RecordAudio(){

    InitAd();
	while(1){
	DACR = ReadAd()<<6;
	}   
}

void SetSleepMode(){

	 while(1){
	 INTWAKE |= (1<<7);
	   LCDPutStr("Modo Normal", 50, 30, LARGE, BLUE, WHITE);
	 if(HOME_BUTTON()){
	     LCDPutStr("Modo PD", 50, 30, LARGE, BLUE, WHITE);
	     Delay(100);
	     PCON = 2;
	 }

	}
}

int main (void) {			 
  
  GPIO_Init();
  init_VIC();
  InitUart();
  
  //fSW_SendString("AT+BTSLP",8);
  //fRA_SendChar(0x0D);
  //while(1){}

  InitTimer();
  //Reset_Bluetooth_Chip();
  InitLCD();

  //SetSleepMode();
  telaInicial();
  Set_CmdMode();
  
  while(TRUE){
      Connect(RFCOM_PORT10);
	  LCDClearScreen(WHITE);
      if(!BASE_NOT_FOUND){
	      UART1_Clear();
          while(!AUTHENTICATED){
              if(Authentication()){
	              LCDClearScreen(WHITE);
				  while(TRUE){
	 	          
				  Change_Screen(IMG_CODE);
				  }
              }else {
			     // Mensagem de autenticacao nao realizada?!?!?!
			     Tela_msg_Autenticando();
			  }
         }
     }else{
	      BASE_NOT_FOUND = FALSE;
	      Tela_msg_baseLigada();
          Delay(5000);
		  Tela_msg_Conectando();
     }
  } 
  return 0;

}

  
  
  