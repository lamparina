#define CHAR	char
#define BOOL unsigned int

/*********************************
Headers do protocolo da Aplicacao
*********************************/

#define M_SEND_KEY '0' //0000
#define M_ACK_KEY '1'//0001
#define M_MOVE '2'//0010
#define	M_BUTTON '3'//0011
#define M_SCREEN_IMG '4'//0100
#define M_SCREEN_COD '5'//0101
#define M_AUDIO '6'
#define M_VIDEO '8'
#define M_IMAGE '9'
#define M_SEC_SEQ1 "#$%" // 450 pacotes de 1k
#define M_SEC_SEQ2 "%$#" // 450 pacotes de 1k
#define M_IMAGE_SEQ_INV "%$##$%"
#define M_ACK 'A'
#define M_ACK_SEQ "A&*@@*&"
#define U 'U' // Move up
#define D 'D' // Move down
#define L 'L' // Move left
#define R 'R' // Move Right
#define OK_C "OK"
#define ALT "AT"
#define HOME "HO"
#define B1 "B1"
#define B2 "B2"
#define B3 "B3"
#define B4 "B4"
#define B5 "B5"
#define B6 "B6"



#define IMG_SIZE "050"//"500"//600  //Tamanho em pacotes
//#define PACKET_SIZE 33//1024
#define IMG_PACKET_SIZE 1024

#define SIZE_1 "0006"
#define SIZE_2 "0016"
#define SIZE_3 "0009"
#define SIZE_4 "1024"
#define SIZE_5 "0001"
#define SIZE_6 "0002"

#define SEND_ACK_KEY 0
#define SEND_KEY 1
#define AUTHENTICATION_DONE 2

// C�digos das imagens do display
#define MENU_FILES_CODE 0
#define SCREEN_PICTURES_CODE 1

extern BOOL CHANGE_IMAGE;
extern short IMG_CODE;
extern BOOL AUTHENTICATED;

void Send_Move(char direction);
void Send_ENTER(void);
void Send_Audio(void);
void Send_Image(const char *img, int size);
void Copy_Screen_Img(CHAR msg[]);
void Copy_Img_Code(CHAR msg[]);
void Receive_MSG(void);
BOOL Authentication(void);
void Send_Button(char button);

