#ifndef __TYPE_H__
#define __TYPE_H__

#ifndef NULL
#define NULL    ((void *)0)
#endif

#ifndef FALSE
#define FALSE   (0)
#endif

#ifndef TRUE
#define TRUE    (1)
#endif

typedef unsigned char  BYTE;
typedef unsigned short WORD;
typedef unsigned long  DWORD;
typedef unsigned char  BOOL;
//#define	BYTE unsigned char
//#define	WORD unsigned short
//#define	DWORD unsigned long
//#define	BOOL unsigned char
							  
#endif  /* __TYPE_H__ */
