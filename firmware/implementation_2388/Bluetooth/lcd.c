#include "LPC23xx.h"			
#include "spi.h"
#include "lcd.h"
#include "type.h"
#include "timer.h"
#include "fonte.h"
#include <string.h>
#include <math.h>
#include <stdlib.h>

void ResetLCD(void)
{
	PINSEL7 &= 0xFFF3FFFF;
  	FIO3DIR |= 0x01000000;
	
	FIO3CLR3 |= 0x01;
	Delay(10);
	FIO3SET3 |= 0x01;		   
}

void enableLCD (void)
{
    PINSEL3 &= P_SSEL;
    IODIR1 |= CS;	 
    IOCLR1 |= CS;	
}

void lightOn (void)
{										 
	PINSEL3 &= LCD_LIGHT;
    IODIR1 |= LCD_LIGHT_CTRL;	 
    IOSET1 |= LCD_LIGHT_CTRL;
}

void lightOff (void)
{
	PINSEL3 &= LCD_LIGHT;
    IODIR1 |= LCD_LIGHT_CTRL;	 
    IOCLR1 |= LCD_LIGHT_CTRL;
}

void InitLCD( void )
{	
    Delay(250);
	ResetLCD();
	enableLCD();
	InitSSP0();
	lightOn();

	// Display control
	SendLCD(LCDCommand,DISCTL);
	SendLCD(LCDData,0x00); // P1: 0x00 = 2 divisions, switching period=8 (default)
	SendLCD(LCDData,0x20); // P2: 0x20 = nlines/4 - 1 = 132/4 - 1 = 32)
	SendLCD(LCDData,0x00); // P3: 0x00 = no inversely highlighted lines
	// COM scan
	SendLCD(LCDCommand,COMSCN);
	SendLCD(LCDData,1); // P1: 0x01 = Scan 1->80, 160<-81
	// Internal oscilator ON
	SendLCD(LCDCommand,OSCON);
	// Sleep out
	SendLCD(LCDCommand,SLPOUT);
	// Power control
	SendLCD(LCDCommand,PWRCTR);
	SendLCD(LCDData,0x0f); // reference voltage regulator on, circuit voltage follower on, BOOST ON
	// Inverse display
	SendLCD(LCDCommand,DISINV);
	// Data control
	SendLCD(LCDCommand,DATCTL);
	SendLCD(LCDData,0x01); // P1: 0x01 = page address inverted, column address normal, address scan in column direction
	SendLCD(LCDData,0x00); // P2: 0x00 = RGB sequence (default value)
	//SendLCD(LCDData,0x01); // P3: 0x01 = Grayscale -> 8 (selects 8-bit color, type B)
	SendLCD(LCDData,0x02); // P3: 0x02 = Grayscale -> 16 (selects 12-bit color, type A)
	// Voltage control (contrast setting)
	SendLCD(LCDCommand,VOLCTR);
	SendLCD(LCDData,32); // P1 = 32 volume value (experiment with this value to get the best contrast)
	SendLCD(LCDData,3); // P2 = 3 resistance ratio (only value that works)
	// allow power supply to stabilize
	Delay(10);
	// turn on the display
	SendLCD(LCDCommand,DISON);

	LCDClearScreen(WHITE);
	//LCDClearScreen(WHITE);
		     
}
/*
void Delay(){
	
	int temp = 10000000;
	while(temp-- > 0);
}
*/
void SendLCD(unsigned char type, unsigned int data) 
{
    if(type)
    {
        data |=0x0100;                                      //seta o nono bit para enviar um comando para o LCD
    }

 	sendSSP0(data);
}

char* itoa(int val, int base)
{
	static char buf[32] = {0};
	int i = 30;
	for(; val && i ; --i, val /= base)
	buf[i] = "0123456789abcdef"[val % base];
	return &buf[i+1];
}


void LCDClearScreen(int color) 
{
	int i; // loop counter
	// Row address set (command 0x2B)
	SendLCD(LCDCommand,PASET);
	SendLCD(LCDData,0);
	SendLCD(LCDData,131);
	// Column address set (command 0x2A)
	SendLCD(LCDCommand,CASET);
	SendLCD(LCDData,0);
	SendLCD(LCDData,131);
	// set the display memory to BLACK
	SendLCD(LCDCommand,RAMWR);
	for(i = 0; i < ((131 * 131) / 2); i++)
	{
		SendLCD(LCDData,(color >> 4) & 0xFF);
		SendLCD(LCDData,((color & 0xF) << 4) | ((color >> 8) & 0xF));
		SendLCD(LCDData,color & 0xFF);
	}
}

void LCDWritebmp(unsigned char figura[],int tam,int pos_ini_x,int pos_ini_y,unsigned char x, unsigned char y) 
{

	int j = 0; // loop counter
	int	tamImage = tam;//(int)sizeof(figura);

	// Data control (need to set "normal" page address for Olimex photograph)
	SendLCD(LCDCommand,DATCTL);
	SendLCD(LCDData,0x00); // P1: 0x00 = page address normal, column address normal, address scan in column direction
	SendLCD(LCDData,0x01); // P2: 0x00 = RGB sequence (default value)
	SendLCD(LCDData,0x02); // P3: 0x02 = Grayscale -> 16
	// Display OFF
	SendLCD(LCDCommand,DISOFF);
	// Column address set (command 0x2A)
	SendLCD(LCDCommand,CASET);
	SendLCD(LCDData,pos_ini_y);
	SendLCD(LCDData,pos_ini_y+y-1);
	// Page address set (command 0x2B)
	SendLCD(LCDCommand,PASET);
	SendLCD(LCDData,pos_ini_x);
	SendLCD(LCDData,pos_ini_x+x-1);
	// WRITE MEMORY
	SendLCD(LCDCommand,RAMWR);
	
    

	for(j = 0; j < tamImage; j++)
	{
	
		//SendLCD(LCDData,tela_navg_arq[j]);
		SendLCD(LCDData,figura[j]);
	
	}

	// Data control (return to "inverted" page address)
	SendLCD(LCDCommand,DATCTL);
	SendLCD(LCDData,0x01); // P1: 0x01 = page address inverted, column address normal, address scan in column direction
	SendLCD(LCDData,0x00); // P2: 0x00 = RGB sequence (default value)
	SendLCD(LCDData,0x02); // P3: 0x02 = Grayscale -> 16
	// Display On
	SendLCD(LCDCommand,DISON);
}

void LCDSetPixel(int x, int y, int color) {
	// Row address set (command 0x2B)
	SendLCD(LCDCommand,PASET);
	SendLCD(LCDData,x);
	SendLCD(LCDData,x);
	// Column address set (command 0x2A)
	SendLCD(LCDCommand,CASET);
	SendLCD(LCDData,y);
	SendLCD(LCDData,y);
	// Now illuminate the pixel (2nd pixel will be ignored)
	SendLCD(LCDCommand,RAMWR);
	SendLCD(LCDData,(color >> 4) & 0xFF);
	SendLCD(LCDData,((color & 0xF) << 4) | ((color >> 8) & 0xF));
	SendLCD(LCDData,color & 0xFF);
}

void LCDSetLine(int x0, int y0, int x1, int y1, int color) {
	
	int dy = y1 - y0;
	int dx = x1 - x0;
	int stepx, stepy;
		
		if (dy < 0) 
		{
			dy = -dy; 
			stepy = -1;  
		}else  
			stepy = 1; 
		
		if (dx < 0)
		{ 
			dx = -dx; 
			stepx = -1;  
		}else 
			stepx = 1;
	
	dy <<= 1; // dy is now 2*dy
	dx <<= 1; // dx is now 2*dx
	
	LCDSetPixel(x0, y0, color);
	
	if (dx > dy) 
	{
		int fraction = dy - (dx >> 1); // same as 2*dy - dx
		while (x0 != x1) 
		{
			if (fraction >= 0) 
			{
				y0 += stepy;
				fraction -= dx; // same as fraction -= 2*dx
			}
		x0 += stepx;
		fraction += dy; // same as fraction -= 2*dy
		LCDSetPixel(x0, y0, color);
		}
	} else {
	
		int fraction = dx - (dy >> 1);
		while (y0 != y1)
		{
			if (fraction >= 0) 
			{
				x0 += stepx;
				fraction -= dy;
			}
		y0 += stepy;
		fraction += dx;
		LCDSetPixel(x0, y0, color);
		}
	}
}

void LCDSetRect(int x0, int y0, int x1, int y1, unsigned char fill, int color) {
	int xmin, xmax, ymin, ymax;
	int i;
	// check if the rectangle is to be filled
	if (fill == FILL)
	{
		// best way to create a filled rectangle is to define a drawing box
		// and loop two pixels at a time
		// calculate the min and max for x and y directions
		xmin = (x0 <= x1) ? x0 : x1;
		xmax = (x0 > x1) ? x0 : x1;
		ymin = (y0 <= y1) ? y0 : y1;
		ymax = (y0 > y1) ? y0 : y1;
		// specify the controller drawing box according to those limits
		// Row address set (command 0x2B)
		SendLCD(LCDCommand,PASET);
		SendLCD(LCDData,xmin);
		SendLCD(LCDData,xmax);
		// Column address set (command 0x2A)
		SendLCD(LCDCommand,CASET);
		SendLCD(LCDData,ymin);
		SendLCD(LCDData,ymax);
		// WRITE MEMORY
		SendLCD(LCDCommand,RAMWR);
		// loop on total number of pixels / 2
		for (i = 0; i < ((((xmax - xmin + 1) * (ymax - ymin + 1)) / 2) + 130); i++) 
		{
			// use the color value to output three data bytes covering two pixels
			SendLCD(LCDData,(color >> 4) & 0xFF);
			SendLCD(LCDData,((color & 0xF) << 4) | ((color >> 8) & 0xF));
			SendLCD(LCDData,color & 0xFF);
		}
	} else {
		// best way to draw un unfilled rectangle is to draw four lines
		LCDSetLine(x0, y0, x1, y0, color);
		LCDSetLine(x0, y1, x1, y1, color);
		LCDSetLine(x0, y0, x0, y1, color);
		LCDSetLine(x1, y0, x1, y1, color);
	}
}

void LCDPutChar(char c, int x, int y, int size, int fColor, int bColor) {
	extern const unsigned char FONT6x8[97][8];
	extern const unsigned char FONT8x8[97][8];
	extern const unsigned char FONT8x16[97][16];
	int i,j;
	unsigned int nCols;
	unsigned int nRows;
	unsigned int nBytes;
	unsigned char PixelRow;
	unsigned char Mask;
	unsigned int Word0;
	unsigned int Word1;
	unsigned char *pFont;
	unsigned char *pChar;
	unsigned char *FontTable[] = {(unsigned char *)FONT6x8, (unsigned char *)FONT8x8, (unsigned char *)FONT8x16};
	// get pointer to the beginning of the selected font table
	pFont = (unsigned char *)FontTable[size];
	// get the nColumns, nRows and nBytes
	nCols = *pFont;
	nRows = *(pFont + 1);
	nBytes = *(pFont + 2);
	// get pointer to the last byte of the desired character
	pChar = pFont + (nBytes * (c - 0x1F)) + nBytes - 1;
	// Row address set (command 0x2B)
	SendLCD(LCDCommand,PASET);
	SendLCD(LCDData,x);
	SendLCD(LCDData,x + nRows - 1);
	// Column address set (command 0x2A)
	SendLCD(LCDCommand,CASET);
	SendLCD(LCDData,y);
	SendLCD(LCDData,y + nCols - 1);
	// WRITE MEMORY
	SendLCD(LCDCommand,RAMWR);
	// loop on each row, working backwards from the bottom to the top
	for (i = nRows - 1; i >= 0; i--) {
		// copy pixel row from font table and then decrement row
		PixelRow = *pChar--;
		// loop on each pixel in the row (left to right)
		// Note: we do two pixels each loop
		Mask = 0x80;
		for (j = 0; j < nCols; j += 2) {
		// if pixel bit set, use foreground color; else use the background color
		// now get the pixel color for two successive pixels
			if ((PixelRow & Mask) == 0)
				Word0 = bColor;
			else
				Word0 = fColor;
			Mask = Mask >> 1;
			if ((PixelRow & Mask) == 0)
				Word1 = bColor;
			else
				Word1 = fColor;
			Mask = Mask >> 1;
			// use this information to output three data bytes
			SendLCD(LCDData,(Word0 >> 4) & 0xFF);
			SendLCD(LCDData,((Word0 & 0xF) << 4) | ((Word1 >> 8) & 0xF));
			SendLCD(LCDData,Word1 & 0xFF);
		}
	}
	// terminate the Write Memory command
	SendLCD(LCDCommand,NOP);
}

void LCDPutStr(char *pString, int x, int y, int Size, int fColor, int bColor) {
	// loop until null-terminator is seen
	while (*pString != 0x00) {
		// draw the character
		LCDPutChar(*pString++, x, y, Size, fColor, bColor);
		// advance the y position
		if (Size == SMALL)
			y = y + 6;
		else if (Size == MEDIUM)
			y = y + 8;
		else
			y = y + 8;
		// bail out if y exceeds 131
		if (y > 131) break;
	}
}	

