#ifndef __UARTDEF__
#define __UARTDEF__

#define UCHAR	unsigned int
#define UINT	unsigned int
//#define BOOL unsigned int
//#define BYTE unsigned int
//#define TRUE	1
///#define FALSE	0
#define CHAR	char
//#define	BYTE	unsigned char
//#define WORD	unsigned int

#define TX1_FIFO_FULL()		((U1LSR & 0x20) == 0x00)//((U1LSR & 0x40) == 0x00)//
//#define UART0_CONFIG			0x50 
#define UART_EIGHT_BITS			0x03
#define UART_TWO_STOP_BITS		0x04
#define UART_ONE_STOP_BITS		0x00
#define UART_DLAB				0x80
#define UART_ENABLE_PARITY		0x08
#define UART_PARITY_EVEN		0x10
#define UART_TRIGGER_LEVEL_0	0x00
#define UART_TRIGGER_LEVEL_1	0x40
#define UART_TRIGGER_LEVEL_3	0xC0
#define	UART_RESET_TX_FIFO		0x04
#define	UART_RESET_RX_FIFO		0x02
#define UART_FIFO_ENABLE		0x01
#define UART_RX_INT_ENABLE		0x01//0x04////0x40 //
//#define PCLK					(unsigned long)14745600
#define BAUD_RATE				(unsigned long)115200//115200//9600 
#define UART_1_Priority					0x01//0x04
#define	INT_UART1_ENABLE   	0X80//0x80
#define RX_BUFFER_LENGHT 	172//100 //20
#define fRA_NIBLE_H(valor) RA_INT_TO_HEX[valor>>4]
#define fRA_NIBLE_L(valor) RA_INT_TO_HEX[valor & 0x0F]
#define TX1_FIFO_EMPTY()	((U1LSR & 0x20) == 0x20)//((U1LSR & 0x40) == 0x40)//
#define RX_DATA_READY()		(U1LSR & 0x01 == 0x01)
#define COMMAND_TIME_OUT_VALUE	400

//definicao das variaveis 
extern char mcSW_RxBuffer[RX_BUFFER_LENGHT];
extern BYTE bSW_NRXBytes;
extern BYTE bPtrSW_RxBufferRD;
extern BYTE bPtrSW_RxBufferWR;
extern BYTE bSW_Time_Out_Cmd;
//extern int qtd_byte_recebidos;
extern int PACKET_RECEIVED;
extern int DATA_MODE;
extern int INT_ENABLE;


void fRA_SendHex(BYTE cDado);
void fRA_SendChar(CHAR cDado);
void fRA_SendChar2(CHAR cDado);
void fRA_SendShort(short pDado);
BOOL fSW_GetCharRX(CHAR *mcBuffer);
void fSW_SendString(CHAR *mcBuffer, unsigned short int  numberData);
BOOL InitUart(void);
void UART1_Disable_Interurpt(void);
void UART1_Enable_Interurpt(void);
void Uart_RXInterrupt(void)__irq;
int GetCharRX(CHAR mcBuffer[]);
void UART1_Clear(void);
#endif
