//#include "defines.h"
#include "lpc23xx.h"
#include "type.h"
#include "timer.h"
#include "uart.h"
#include "irq.h"




int w = 0;
int TIME0=0;
int TIME1=0;
int TIME2=0;
int TIME3=0;
int TIMEB=0;


BOOL InitTimer(void){
   if(install_irq(TIMER0_INT, (void *) Timer0_Interrupt, TIMER_0_Priority )){
       return TRUE;
   }else {
	   return FALSE;
   } 
}

void set_TIME(int value, int timer){
  if(timer==0){
     TIME0 = value;
  }else if(timer==1){
     TIME1 = value;
  }else if(timer==2){
     TIME2 = value;
   }else if(timer==3){
     TIME3 = value;
   }else if(timer==4){
     TIMEB = value;
   }
}

int get_TIME(int timer){
	int retorno;
	 if(timer==0){
     retorno = TIME0;
  }else if(timer==1){
     retorno = TIME1;
  }else if(timer==2){
     retorno = TIME2;
   }else if(timer==3){
     retorno = TIME3;
   }else if(timer==4){
     retorno = TIMEB;
   } 
	return retorno;
}



void LigaRelogio(void){

	T0TCR = DISABLE_COUNTER;		// desabilita contador e preescaler
	T0TC  = CLEAR_COUNTER;   		//zera contador
	T0PR  = PREESCALER_VALUE;  		// configura  preescaler
	T0PC  = CLEAR_COUNTER;  		// zera contador de preescaler
	T0MR0 = MATCH_VALUE_1MS;	 	//tempo de 1ms
	T0MCR = ENABLE_INTERRUPT + RESET_COUNTER;// 0x03;   	// habilita interrupção e resete do contador no fim da contagem 
	T0TCR = ENABLE_COUNTER;   	// inicia contagem	 */
	
}

/****************************************************************************
**
**   Funcao:  	fLSC_desligaRelogio()
**
**   Descricao: Funcao que desliga o timer do SC
**
**   Parametros de Entrada: Nenhum					 
**
**   Parametros de Saida: Nebhum
**
***************************************************************************/

void DesligaRelogio(void){
///
	T0TCR = DISABLE_COUNTER;		// desabilita contador e preescaler
	T0MCR = DISABLE_INTERRUPT;   // desabilita interrupção
}

void LigaTimer(void){

	T1TCR = DISABLE_COUNTER;		// desabilita contador e preescaler
	T1TC  = CLEAR_COUNTER;   		//zera contador
	T1PR  = PREESCALER_VALUE;  		// configura  preescaler
	T1PC  = CLEAR_COUNTER;  		// zera contador de preescaler
	T1MR0 = MATCH_VALUE_5uS;		//tempo de 5us
	T1MCR = ENABLE_INTERRUPT + RESET_COUNTER;  	// habilita interrupção e resete do contador no fim da contagem 
	T1TCR = ENABLE_COUNTER;   	// inicia contagem	 */
	
}

void DesligaTimer(void){
	T1TCR = DISABLE_COUNTER;		// desabilita contador e preescaler
	T1MCR = DISABLE_INTERRUPT;   // desabilita interrupção
}

/****************************************************************************
**
**   Funcao:  	fSW_Turn_on_Timer()
**
**   Descricao: Funcao que liga o timer1
**
**   Parametros de Entrada: BYTE bTimer - tempo de captura (escala de tempo)
**							
**
**   Parametros de Saida: Nenhum
**
****************************************************************************/
void fSW_Turn_on_Timer(BYTE bTimer){

	T1TCR = DISABLE_COUNTER;		// desabilita contador e preescaler
	T1TC  = CLEAR_COUNTER;   		//zera contador
	T1PR  = PREESCALER_VALUE;  		// configura  preescaler
	T1PC  = CLEAR_COUNTER;  		// zera contador de preescaler
	T1MR0 = (PCLK * TIME_BASE)/((1 + PREESCALER_VALUE )* bTimer); 	//tempo de 1ms
	T1MCR = ENABLE_INTERRUPT + RESET_COUNTER;// 0x03;   	// habilita interrupção e resete do contador no fim da contagem 
	T1TCR = ENABLE_COUNTER;   	// inicia contagem	 

}

void Delay(int temp){
	TIME0 = temp;
	//w = temp;
	LigaRelogio();
	//LigaTimer();
	while(TIME0);
	//DesligaTimer();
	DesligaRelogio();
}
/****************************************************************************
**
**   Funcao:  	fSW_Turn_off_Timer()
**
**   Descricao: Funcao que desliga o timer1
**
**   Parametros de Entrada: Nenhum
**							
**
**   Parametros de Saida: Nenhum
**
****************************************************************************/
void fSW_Turn_off_Timer(){
	T1TCR = DISABLE_COUNTER;		// desabilita contador e preescaler
	T1MCR = DISABLE_INTERRUPT;   // desabilita interrupção
}

 
void Timer0_Interrupt(void)__irq
{
	T0IR = 0x01;  //limpa flag da interrupção
	IENABLE;
		
  	if (TIME0)	
		TIME0--;
	if (TIME1)	
		TIME1--;
	if (TIME2)	
		TIME2--;
   	if (TIME3)	
		TIME3--;
	if (TIMEB)	
		TIMEB--;
   	IDISABLE;
	VICVectAddr = 0;
}

void Timer1_Interrupt(void)__irq
{
	T1IR = 0x01;  //limpa flag da interrupção
	IENABLE;
		
  	if (w)	// Time out entre bytes de uma resposta, tempo máximo entre bytes de uma resposta 
		w--;
   
   	IDISABLE;
	VICVectAddr = 0;
}
