
#include <stdio.h>
#include <LPC23xx.H>                    /* LPC23xx definitions                */
#include "type.h"
#include "ApplicationProtocol.h" 
#include "uart.h"
#include "SerialBluetooth.h"
#include "timer.h"
#include "GPIO.h"
#include "telas.h"
#include "lcd.h"

#include <string.h>

BOOL BASE_NOT_FOUND = FALSE;
int N_DEVICES_DISCOVERED =0;

char OK[4] = {0x0D,0x0A,'O','K'};
char ERROR[7] = {0x0D,0x0A,'E','R','R','O','R'};
char LAMPEJO_F[9] = {'"','l','a','m','p','e','j','o','"'};
//char LAMPEJO_F[11] = {'"','l','a','m','p','e','j','o','-','0','"'};

/*********************************
Buffers auxiliares
*********************************/
char ADDRESS[12];
char INQUIRY_BUFFER[172];	// Suporta 5 dispositivos
char ANSWERS_BUFFER[100]; // Que tamanho colocar ??


/***********************************************************************************
**
** Funcao: SearchAnswer(char buffer[], char ans[], int tam_buffer, int tam_ans)
**
** Descricao: Funcao auxiliar que compara o conteudo de dois buffers. 
** Utilizada para verificar as respostas de OK e ERROR vindas do chip bluetooth
** BGB203.
**
** Parametros de Entrada: char buffer[] - buffer contendo a mensagem recebida do chip
					      char ans[] - buffer conteudo o conteudo esperado (ex: OK ou ERROR)
						  int tam_buffer - tamanho do primeiro buffer
						  int tam_ans - Tamanho do segundo buffer
**							
**
**   Parametros de Saida: Booleano "found" indicando se a resposta passada no buffer ans[]
						  foi encontrada no buffer[].	
**
***********************************************************************************/
BOOL SearchAnswer(char buffer[], char ans[], int tam_buffer, int tam_ans){
	short index=0;
	int i=0;
	int tam_a = tam_ans;
	int size = tam_buffer; 
	BOOL found = FALSE;
	for(i=0;i<size && !found;i++){
  		if(ans[index] == buffer[i]){
			index++;			
			if(index == tam_a)
			   found = TRUE;
		}else{
		 index =0;	
		}	
    }
	return found;
}
/***********************************************************************************
**
** Funcao: CatchAnswer()
**
** Descricao: Funcao auxiliar que roda at� encontrar ums mensagem de ERRO ou OK,
   enviada pelo chip Bluetooth. As mensagens s�o procuradas no buffer mcSW_RxBuffer
   que armazenas os dados que chegam da UART
** 
** 
**
** Parametros de Entrada: 
**							
**
**   Parametros de Saida: Booleano "ans" indicando se a resposta passada encontrada
						  foi OK (TRUE) ou ERROR (FALSE);
**
***********************************************************************************/


BOOL CatchAnswer(void) {
	BOOL ans;
	BOOL stop=0;
	while(!stop){
	  if(SearchAnswer(mcSW_RxBuffer,OK, bPtrSW_RxBufferWR,(sizeof OK))){
	      ans = 1;
		  stop = 1;
	  }else if(SearchAnswer(mcSW_RxBuffer,ERROR, bPtrSW_RxBufferWR,(sizeof ERROR))){
	      ans = 0;
		  stop = 1;
	  }
	}
	return ans;
}

/*
BOOL Pairing(){
  UART1_Clear();
  fSW_SendString(PAIRING, 11); 
  fSW_SendString(ADDRESS, 12);
  fSW_SendString(KEY, 16);  
  fRA_SendChar(0x0D);
  if(CatchAnswer()){
	GetCharRX(ANSWERS_BUFFER); 
	//if(CompareKey()){
  	//    return TRUE;
	//}else{
	  //  return FALSE;
	//} 
  }else{
    return FALSE;
  }

}
*/

/***********************************************************************************
**
** Funcao: Set_EchoMode()
**
** Descricao: Funcao auxiliar seta o modo ECHO do chip Bluetooth BGB203
** 	 
**
** Parametros de Entrada: BOOL mod - booleano indicando ativa��o ou desativa��o
									 do modo ECHO. 
**							
**
**   Parametros de Saida: Booleano indicando se a opera��o foi realizada
						  com sucesso.
**
***********************************************************************************/

BOOL Set_EchoMode(BOOL mode){
	UART1_Clear();
    if(mode){
	    fSW_SendString(ECHO_ON,4);  
  	    fRA_SendChar(0x0D);
	}else {
		fSW_SendString(ECHO_OFF,4);  
  		fRA_SendChar(0x0D); 
	}
	if(CatchAnswer()){
  		//GetCharRX(buffer);
  		return TRUE; 
  	}else{
   		return FALSE;
  	}
}

/***********************************************************************************
**
** Funcao: GetRmtName(char buffer[])
**
** Descricao: Funcao respons�vel por descobrir o nome do dispositivo remoto com
			  endere�o armazenado no buffer auxiliar ADDRESS.
** 
** 
**
** Parametros de Entrada: char buffer[] - buffer que armazena o nome do dispositivo
						  remoto. 
**							
**
**   Parametros de Saida: Booleano indicando se a opera��o foi realizada
						  com sucesso.
**
***********************************************************************************/


BOOL GetRmtName(char buffer[]){
  
  UART1_Clear();
  fSW_SendString(GET_NAME, 9); 
  fSW_SendString(ADDRESS, 12); 
  fRA_SendChar(0x0D);
  if(CatchAnswer()){
	GetCharRX(buffer);
  	return TRUE; 
  }else{
    return FALSE;
  }
}

/***********************************************************************************
**
** Funcao: ConnectSPP(char port[])
**
** Descricao: Funcao respons�vel por realizar uma conexa
** 
** 
**
** Parametros de Entrada: char port[] - buffer contendo a porta RFCOM a ser utilizada
			              na conex�o mais um caracter ',' (virgula) que faz parte
						  do comando AT de conexao.
**							
**
**   Parametros de Saida: Booleano indicando se a opera��o foi realizada
						  com sucesso.
**
***********************************************************************************/

BOOL ConnectSPP(char port[]){
	UART1_Clear();
	fSW_SendString(CONNECT_SPP_CLT,9);
	fSW_SendString(ADDRESS,12);
	fSW_SendString(port,3);
	fRA_SendChar(0x0D);
	
	if(CatchAnswer()){
  		//GetCharRX(buffer);
  		return TRUE;
		 
  	}else{
   		return FALSE;
  	}
}

/***********************************************************************************
**
** Funcao: Inquiry(char buffer[])
**
** Descricao: Funcao respons�vel por realizar uma busaca de 2s por dispositivos
**	          bluetooh que estejam ao alcance do controle. Armazena no buffer
**			  o endere�o do 5 primeiros dispositivos encontrados. 
** 
** 
**
** Parametros de Entrada: char buffer[] - buffer que armazena os endere�os dos 5
						  primeiros dispositivos encontrados	
**							
**
** Parametros de Saida: Booleano indicando se a opera��o foi realizada
**						com sucesso (OK ou ERROR vindos do chip Bluetooth).
**
***********************************************************************************/

BOOL Inquiry(char buffer[]){
	UART1_Clear();
	fSW_SendString(INQUIRY,10);
	fRA_SendChar(0x0D);
	if(CatchAnswer()){
  		if((bPtrSW_RxBufferWR>22)){
		    N_DEVICES_DISCOVERED = (bPtrSW_RxBufferWR -22)/30;
		}else{
		    N_DEVICES_DISCOVERED = 0;
		}
		GetCharRX(buffer);
  	    // Tamanho do buffer do INQUIRY = 30x +22 (x numero de disp.)	
		if(N_DEVICES_DISCOVERED >5){
		  N_DEVICES_DISCOVERED =5;
		}
		return TRUE; 
  	}else{
   		return FALSE;
	}
	
}		  

/***********************************************************************************
**
** Funcao: SearchBase()
**
** Descricao: Funcao respons�vel por procurar a base (parte fixa doi lampejo).
**			  Realiza um Inquiry dos dispositivos em volta e se conecta com o
**			  dispositivo remoto com o nome de LAMPEJO_F.
** 
** 
**
** Parametros de Entrada: char buffer[] - buffer que armazena os endere�os dos 5
**						  primeiros dispositivos encontrados	
**							
**
** Parametros de Saida: Booleano indicando se a conex�o com a base foi realizada
						ou n�o.
**
***********************************************************************************/

BOOL SearchBase(void){
	int i=0;
	int k=0;
	int index=10; // Primeiro numero do endereco na resposta do INQUIRY!!!
	BOOL found =FALSE;
	if(Inquiry(INQUIRY_BUFFER)){
	    for(k=0;k<N_DEVICES_DISCOVERED && !found;k++){ 
		    for(i=0;i<ADDRESS_SIZE;i++){
		        ADDRESS[i] = INQUIRY_BUFFER[index];
		        index++;
		    }
		GetRmtName(ANSWERS_BUFFER);
		found = SearchAnswer(ANSWERS_BUFFER,LAMPEJO_F,(sizeof ANSWERS_BUFFER),(sizeof LAMPEJO_F));
		index = index - ADDRESS_SIZE;
		index = index+30; //Pula para o prox endereco no INQIRY_BUFFER!!!
		}
 	 }else{
	 	return FALSE;
	 }
	if(found){
	    return TRUE;	
	}else{
	    return FALSE;
	}
}

/***********************************************************************************
**
** Funcao: Set_CmdMode()
**
** Descricao: Funcao respons�vel por configurar o chip Bluetooth BGB203 para
** 			  funcionar no modo comando.
** 
**
** Parametros de Entrada: 
**							
**
** Parametros de Saida: Booleano indicando se a opera��o foi realizada
**						com sucesso (OK ou ERROR vindos do chip Bluetooth).
**
***********************************************************************************/

BOOL Set_CmdMode(void){
	UART1_Clear();
	fSW_SendString(ESC_SEQ,3);
	Delay(100);
	//if(CatchAnswer()){
 // 	  GetCharRX(buffer);
  	return TRUE; 
  	//}else{
   	//	return FALSE;
  	//}
}			  


void Reset_Bluetooth_Chip(void){

   //RESET_LOW_BLUETOOTH();
   Delay(10);
   //RESET_HIGH_BLUETOOTH();
}
/***********************************************************************************
**
** Funcao: Connect(char port[])
**
** Descricao: Funcao respons�vel realizar conex�o com a base na porta RFCOM passada
			  como parametro. Essa fun��o chama todas as fun��es de conex�o como
			  Inquiry, SearchBase, ConnectSPP. Tenta se conectar com a base 10 vezes. 
** 
** 
**
** Parametros de Entrada: char port[] - buffer contendo a porta RFCOM a ser utilizada
**			              na conex�o mais um caracter ',' (virgula) que faz parte
**						  do comando AT de conexao.
**							
**
** Parametros de Saida: Booleano indicando se a conex�o com a base foi realizada
						ou n�o.
**
***********************************************************************************/

BOOL Connect(char port[]){
	  
	  int number=0;
	  BOOL TIMEOUT =0;
      //Reset_Bluetooth_Chip();
	  while(!Set_EchoMode(FALSE)){
	  Delay(10); //2ms
	  }
	  Tela_msg_Conectando();
	  while (!SearchBase() && !TIMEOUT){
	      
		  Delay(2); //2ms
	      number++;
	      if(number ==10){
	          TIMEOUT = TRUE;
			  BASE_NOT_FOUND = TRUE;
	          //Tela_msg_baseLigada();
		      //Delay(3000);
		  	  return FALSE;
		  }
	  }
	  if(ConnectSPP(port)){
          DATA_MODE =1;
		  
		  return TRUE;	
	  }else {
	      return FALSE;
      }
    
	//UART1_Clear();
}
BOOL Send_AT (void){
	int counter = 1;
	
	UART1_Clear();
	
	fSW_SendString("AT",2);
	while((counter>0) ){
  	  Delay(10);
	  counter--;
	  if(SearchAnswer(mcSW_RxBuffer,OK, bPtrSW_RxBufferWR,(sizeof OK))){
  	    return TRUE; 
  	  }
	}
	return FALSE;
  	
}

   


/*
int main (void) {			 
 
  	
  InitInterrupt();
  GPIO_Init();
  InitUart();
  Reset_Bluetooth_Chip();
  
  //if(!Send_AT()){
      Set_CmdMode();
  //}
      Connect(RFCOM_PORT10);
      UART1_Clear(); 
      if(Authentication()){
       //Send_Image(logo,50);
	  while(1){
	     if(PACKET_RECEIVED){
	         Receive_MSG();
         }
		 JoystickCommands();
		 Buttons();
        
      }
     }
   return 0;

} */
/*
  fSW_SendString("+++",3);
  fSW_SendString("+++",3);
  fSW_SendString("+++",3);
  fSW_SendString("AT+BTURT=115200,8,0,1,0",23);
  fRA_SendChar(0x0D);
  Delay(2000);
  fSW_SendString("AT+BTURT=115200,8,0,1,0",23);
  fRA_SendChar(0x0D);
*/

//Delay(3000);
  /* while(1){
   //fSW_SendString("5555",4);
   Send_Move(U);
   Delay(1000);
   Send_Move(D);
   Delay(1000);
   Send_Move(R);
   Delay(1000);
   Send_Move(L);
   Delay(1000);
   Send_OK();
  }

  */
