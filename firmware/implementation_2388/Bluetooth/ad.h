#ifndef __ADDEFS__
#define __ADDEFS__
///defines do AD
#define SEL_AD0_4  						(1<<4)
#define SEL_AD0_6							(1<<6)
#define SEL_AD0_2							(1<<2)

#define CLK_DIV_AD_6 					0x0500
#define BURST									0x10000
#define START_CONVERSION			0x01000000
#define STOP_CONVERSION			    0x00000000
 
#define ENABLE_INTERRUPT_AD04	0x00000010
#define DONE_CAPTURE					0x80000000	   
#define AD0_4									0x00C00000 
#define AD0_6									0xC0000
#define AD0_2									0x40000
#define PDN_OPERATIONAL 			0x00200000	
#define ADC_0_Priority				0x05
#define MAX_VALUE_AD					0x03FF
#define ENABLE_ADC (1<<12)
#define INT_AD 0x40000

extern int CATCH_BLOCK;
extern short s_buffer[5120];

BOOL InitAd(void);
void StopAd(void);
int ReadAd(void);
int ConverteAdPixel(void);
void AD_Interrupt(void)__irq;

#endif
