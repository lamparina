#ifndef __SPI_H_
#define __SPI_H_


#define   P_SCK       0x00000300
#define	  P_MISO 	  0x0000C000
#define	  P_MOSI 	  0x00030000
#define	  P_SSEL 	  0xFFFFF3FF



#define   BitEnable   0x00000004
#define   CPHA		  0x00000000
#define   CPOL		  0x00000000
#define   MSTR		  0x00000020
#define   LSBF		  0x00000000
#define   SPIE		  0x00000000
#define   BITS		  0x00000900
#define   SPCLK       8
#define   SPIF        0x00000080
#define   CS    	  0x00200000

void InitSSP0 (void);
void sendSSP0(unsigned int data) ;


#endif


