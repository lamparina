#define BOOL unsigned int
/*******************************
Comando AT utilizados no modo de
comando do chip bluetooth
********************************/
#define INQUIRY  "AT+BTINQ=2"  // Inquiry de 2 segundos */
#define PAIRING   "AT+BTPAR=1," 
#define GET_NAME "AT+BTRNM=" 
#define DATA_MODE_SPP_SRV "AT+BTSRV="
#define CONNECT_SPP_CLT "AT+BTCLT="
#define RFCOM_PORT1 ",1"
#define RFCOM_PORT2 ",2"
#define RFCOM_PORT10 ",10"
#define ESC_SEQ "+++"
#define ECHO_ON "ATE1"
#define ECHO_OFF "ATE0"

/*********************************
Constantes utilizadas
*********************************/
					 
#define ADDRESS_SIZE 12	 // Tamanho do endereco Bluetooth em bytes
#define	NOT_PRESSED 0
#define	NOT_PRESSED2 2
#define PRESSED  1

/*********************************
Variaveis auxiliares
*********************************/
extern BOOL BASE_NOT_FOUND;

BOOL SearchAnswer(char buffer[], char ans[], int tam_buffer, int tam_ans);
BOOL Connect(char port[]);
BOOL CatchAnswer(void);
BOOL Set_EchoMode(BOOL mode);
BOOL GetRmtName(char buffer[]);
BOOL ConnectSPP(char port[]);
BOOL Inquiry(char buffer[]);
BOOL SearchBase(void);
BOOL Set_CmdMode(void);
void Reset_Bluetooth_Chip(void);
BOOL Connect(char port[]);
BOOL Send_AT (void);
