#include "type.h"
#include "ApplicationProtocol.h"
#include "uart.h"
#include "timer.h"
#include "SerialBluetooth.h"
#include <stdlib.h> 


//307201; // bytes + codigo da imagem
//IMAGENS DA TELA
int IMG_INDEX=0;
//char SCREEN_IMG[3000]; // Imagens vao fiacr no SD!
short IMG_CODE = 0;
BOOL ACK_KEY = FALSE;
BOOL ACK_IMG = FALSE;
BOOL KEY_RECEIVED = FALSE;
BOOL CHANGE_IMAGE = FALSE;
BOOL AUTHENTICATED = FALSE;
//char KEY[16] = {'A','D','7','8','0','5','5','5','1','M','E','C','L','4','3','2'};
char KEY[16] = {'1','2','3','4','5','6','7','8','9','0','1','2','3','4','5','6'};
char ACK_KEY_SEQ[7] = {'1','%','$','#','#','$','%'};
char ACK_IMG_SEQ[10] = {'9','#','$','%','0','5','0','%','$','#'};
char KEY_INV[16] = {'1','2','3','4','5','6','7','8','9','0','1','2','3','4','5','6'};



/***********************************************************************************
**
** Funcao: Send_Move(char direction)
**
** Descricao: Funcao respons�vel por enviar comando MOVE para parte fixa, com
** 			  como objetivo de mover o cursor na aplica��o rodando na parte fixa.
**
** Parametros de Entrada: char diretcion - caracter indicando a dire��o na qual o 
                          cursor ser� movido
**							
**
** Parametros de Saida: 
**
***********************************************************************************/

void Send_Move(char direction){

  fRA_SendChar(M_MOVE);
  fSW_SendString(SIZE_5,4); 
  fRA_SendChar(direction);  	
}

void Send_Button(char button){

  fRA_SendChar(M_BUTTON);
  fSW_SendString(SIZE_6,4); 
  switch(button){
      case '1':
	      fSW_SendString(B1,2);
	  break;	   
	  case '2':
	      fSW_SendString(B2,2); 
	  break;
	  case '3':
	      fSW_SendString(B3,2); 
	  break;
	  case '4':
	      fSW_SendString(B4,2); 
	  break;
	  case '5':
	      fSW_SendString(B5,2);
	  break;
	  case '6':
	      fSW_SendString(B6,2);
	  break;
	  case 'A':
		  fSW_SendString(ALT,2);
	  break;
	  case 'H':
		  fSW_SendString(HOME,2);
	  break;
 }
}

/***********************************************************************************
**
** Funcao: Send_OK()
**
** Descricao: Funcao respons�vel por enviar comando OK para parte fixa, comando
              similar ao enter ou dois clicks no teclado.
**
** Parametros de Entrada: 
**							
**
** Parametros de Saida: 
**
***********************************************************************************/

void Send_ENTER(){
  fRA_SendChar(M_BUTTON);
  fSW_SendString(SIZE_6,4); 	
  fSW_SendString(OK_C,2); 		
}
/*
void Send_Audio(void){



}						   
*/

/***********************************************************************************
**
** Funcao: Compare_Key(CHAR key_r[]){
**
** Descricao: Funcao respons�vel por comparar a chave enviada pela parte fixa
			  com a chave armazenada no controle.
**
** Parametros de Entrada: CHAR key_r[] - chave recebida da parte fixa 
**							
**
** Parametros de Saida: Booleano indicando se as chaves s�o iguais ou n�o
**
***********************************************************************************/

BOOL Compare_Key(CHAR key_r[]){
	int i=0;
	BOOL retorno = TRUE;
	for(i=0;i<16;i++){
	    if(key_r[i+1] != KEY_INV[i]){
		    retorno = FALSE;
		} 
	}
    return retorno;
}

/***********************************************************************************
**
** Funcao: Authentication(void)
**
** Descricao: Funcao respons�vel por autentic�o da base no controle.
			  Utiliza a fun��o CompareKey(CHAR key_r[])
**
** Parametros de Entrada: 
**							
**
** Parametros de Saida: Booleano se auntentica��o foi realizada com sucesso
**
***********************************************************************************/

BOOL Authentication(void){
	char state =SEND_ACK_KEY;
	LigaRelogio();
	set_TIME(5000,1);
	while((get_TIME(1)>0) && !AUTHENTICATED){
	    
		switch(state){
		    
			case SEND_ACK_KEY:
			    fRA_SendChar(M_SEND_KEY);
		        fSW_SendString(SIZE_1,4); 	 
	            fSW_SendString(M_SEC_SEQ1,3);
		        fSW_SendString(M_SEC_SEQ2,3); 
		        Delay(500);
	            if(PACKET_RECEIVED){
		            Receive_MSG();
		        }
				if(ACK_KEY){
				    state = SEND_KEY; 
				}
			break;
		   case SEND_KEY: 
		    	ACK_KEY = FALSE;
	            fRA_SendChar(M_SEND_KEY);
		        fSW_SendString(SIZE_2,4); 	 	 
	            fSW_SendString(KEY,16);
	            Delay(500);
	            if(PACKET_RECEIVED){
		            Receive_MSG();
		        }
				if(KEY_RECEIVED){
				  state = AUTHENTICATION_DONE;
				}
		   break;

		   case AUTHENTICATION_DONE:
               AUTHENTICATED = TRUE;
			   set_TIME(0,1); 		   	
		   break;
		}
	 }
		/*if(!ACK_KEY){	
		    fRA_SendChar(M_SEND_KEY);
		    fSW_SendString(SIZE_1,4); 	 
	        fSW_SendString(M_SEC_SEQ1,3);
		    fSW_SendString(M_SEC_SEQ2,3); 
		    Delay(250);
	        if(PACKET_RECEIVED){
		       Receive_MSG();
		    }
		}else{
		 set_TIME(0,0); 
		}
	}		 
	if(ACK_KEY){
	    set_TIME(1000,0); 	 
 	    while((get_TIME(0)>0)){	  
	        if(!KEY_RECEIVED){
	            ACK_KEY = FALSE;
	            fRA_SendChar(M_SEND_KEY);
		        fSW_SendString(SIZE_2,4); 	 	 
	            fSW_SendString(KEY,16);
	            Delay(250);
	            if(PACKET_RECEIVED){
		            Receive_MSG();
		        }
	        }else{
		         set_TIME(0,0);
				 AUTHENTICATED = TRUE; 
	 	    }
		}
	}else{
	     AUTHENTICATED = FALSE; 
	}

	*/
   KEY_RECEIVED = FALSE;
   DesligaRelogio();
   return AUTHENTICATED; // colocar um timeout!!!!	
}

/***********************************************************************************
**
** Funcao: Send_Img_Packet(const short *img, int packet_size)
**
** Descricao: Funcao auxiliar para enviar pacotes de uma imagem
**
** Parametros de Entrada: const short *img - Ponteiro contendo os pixels da imagem 
**						  int packet_size - tamanho do pacote	
**
** Parametros de Saida: 
**
***********************************************************************************/

void Send_Img_Packet(const char *img, int packet_size){
	int i=0;
	const char *buffer;
	buffer = img;
	fRA_SendChar(M_IMAGE);
	fSW_SendString(SIZE_4,4);
	Delay(10);
	for(i=0;i<(packet_size);i++){
	    //fRA_SendShort(*buffer);
		fRA_SendChar(*buffer);
		buffer++;
	}
}

/***********************************************************************************
**
** Funcao: Send_Initial_Packet(CHAR header, char seq[],short size)
**
** Descricao: Funcao auxiliar para enviar pacotes iniciais de qualquer transa��o
			  (enviar imagem, audio, video). 
**
** Parametros de Entrada: CHAR header - Cabe�alho da mensagem
**						  char seq[] - Sequencia corespondente ao cabe�alho
**						  short size - Tamnho em pacotes do arquivo que ser�
						  enviado ap�s a mensagem inicial. 		
**
** Parametros de Saida: 
**
***********************************************************************************/

void Send_Initial_Packet(CHAR header, char seq[],char size[]){ //Mudar Parametros!!!
		//int i=0;
	    fRA_SendChar(M_IMAGE);
		fSW_SendString(SIZE_3,4);	   // Mensagem Inicial
	    fSW_SendString(M_SEC_SEQ1,3); //Indicando a transmissao
	    fSW_SendString(IMG_SIZE,3); //Passa o caracter ou o numero // de uma imagem
        fSW_SendString(M_SEC_SEQ2,3);
		
}

/***********************************************************************************
**
** Funcao: Send_Image(const short *img, int size)
**
** Descricao: Funcao respons�vel enviar uma imagem
**
** Parametros de Entrada: const short *img - Ponteiro contendo os pixels da imagem
**						  int size - Tamnaho em pacotes da imagem
**						  
**
** Parametros de Saida: 
**
***********************************************************************************/

void Send_Image(const char *img, int size){
	int i=0;
    while(!ACK_IMG){
		Send_Initial_Packet(M_IMAGE,M_SEC_SEQ1,IMG_SIZE);
		Delay(500);
		if(PACKET_RECEIVED){
		   Receive_MSG();
		}
	}
	Delay(50);
	ACK_IMG = FALSE;
	for(i=0;i<size;i++){
	    Send_Img_Packet(img,IMG_PACKET_SIZE);
		img = img+ (IMG_PACKET_SIZE); //sizeof short
		Delay(5);
	}
}

/***********************************************************************************
**
** Funcao: Receive_MSG(void)
**
** Descricao: Funcao respons�vel por receber mensagens da parte fixa e decodificar
**		      a mensagem recebida. Aguarda 1/2 seg por uma mensagem com tamanho igual
**			  ap PACKET_SIZE
**
** Parametros de Entrada: 
**						  
**						  
**
** Parametros de Saida: 
**
***********************************************************************************/

void Receive_MSG(void){
   CHAR msg[33];
   char HEADER;	
   	       GetCharRX(msg);
		   HEADER = msg[0];
		   switch(HEADER){
			   case M_SCREEN_IMG: // Mesagem de Tela para o controle
       	           //Copy_Screen_Img(msg);
	   		   break;

               case M_SCREEN_COD: // Mesagem de Tela para o controle
                   Copy_Img_Code(msg);
				   CHANGE_IMAGE = TRUE;
	             
               break;

               case M_ACK_KEY:
		           if(SearchAnswer(ACK_KEY_SEQ,msg,7,7)){
				       ACK_KEY = TRUE;
					  
				   }
		       break;
			   
		       case M_SEND_KEY:
		           if(Compare_Key(msg)){
			          KEY_RECEIVED = TRUE;  
			       }   
		       break;

			   case M_IMAGE:
		           if(SearchAnswer(ACK_IMG_SEQ,msg,10,10)){
			          ACK_IMG = TRUE;   
			       }   
		       break;
		   	   		
               default:
			   UART1_Clear();  
               break;
           }
		   PACKET_RECEIVED =0;
		   UART1_Enable_Interurpt();
		   UART1_Clear();  
		
}

/*
void Copy_Screen_Img(CHAR msg[]){
	int i=0;
	for(i=1;i<PACKET_SIZE;i++){ //1: Nao pega o HEADER
	    SCREEN_IMG[IMG_INDEX] = msg[i];
	    IMG_INDEX++;
	}		
	IMG_INDEX = 0;
}
*/

/***********************************************************************************
**
** Funcao: Copy_Img_Code(CHAR msg[])
**
** Descricao: Funcao respons�vel armazenar o c�digo (enviado pela parte fixa) 
**            da mensagem a ser carregada no display.
**
** Parametros de Entrada: 
**						  
**						  
**
** Parametros de Saida: 
**
***********************************************************************************/

void Copy_Img_Code(CHAR msg[]){
	
	CHAR code[2];
	code[0] = msg[1];
	code[1] = msg[2];	
	IMG_CODE = atoi(code);//pula o cabecalho
	fRA_SendChar(M_SCREEN_COD);
	fSW_SendString(SIZE_6,4);
	fSW_SendString(code,3); // TODO: Verficar o tamanho 3!!!
	
}
