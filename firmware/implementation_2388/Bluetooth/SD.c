#include "lpc23xx.h"
#include "type.h"
#include "lcd.h"
#include "mci.h"
#include "timer.h"
#include "SD.h"


int blocoAtual =0;
volatile BYTE WriteBlock[BLOCK_LENGTH];				 
volatile BYTE ReadBlock[BLOCK_LENGTH];
volatile DWORD TXBlockCounter, RXBlockCounter;
extern volatile DWORD CardType;


int SD_writeImage(const unsigned char * imagem, int tamImagem)
{		
	int i=0, j=0, byteImagem=0,numBlocos=0;
	int blocoFinal=0;
	if (preparaSD()){	
		numBlocos = (tamImagem/512);
		if ((tamImagem%512)>0) numBlocos += 1;
		blocoFinal = numBlocos + blocoAtual; 
		for ( i = blocoAtual; i < blocoFinal; i++ )
	    {
	 		for (j = 0; j<512; j++){
				WriteBlock[j] = 0;
				if (byteImagem<tamImagem)
					WriteBlock[j]=imagem[byteImagem++];
			}	
			Delay(100);
			TXBlockCounter = 0;
	 		if ( MCI_Write_Block( i ) != TRUE )
			{
				while ( 1 );	
			}
			while ( MCI_STATUS & MCI_TX_ACTIVE );
			MCI_TXDisable();
	 	}
	 	//atualizaBlocoAtual(blocoFinal);
		return 0;

	}else {
	
		return 1;
	}
}

int SD_writeSound(short * sound_buffer, int tamBuff)
{		
	int i=0, j=0, byteSound=0,numBlocos=0;
	int blocoFinal=0;
	if (preparaSD()){	
		numBlocos = (tamBuff/512);
		if ((tamBuff%512)>0) numBlocos += 1;
		blocoFinal = numBlocos + blocoAtual; 
		for ( i = blocoAtual; i < blocoFinal; i++ )
	    {
	 		for (j = 0; j<512; j++){
				WriteBlock[j] = 0;
				if (byteSound<tamBuff)
					WriteBlock[j]=sound_buffer[byteSound]>>2;
					//WriteBlock[j+1]=(sound_buffer[byteSound++]>>8);
			}	
			Delay(100);
			TXBlockCounter = 0;
	 		if ( MCI_Write_Block( i ) != TRUE )
			{
				while ( 1 );	
			}
			while ( MCI_STATUS & MCI_TX_ACTIVE );
			MCI_TXDisable();
	 	}
	 	//atualizaBlocoAtual(blocoFinal);
		return 0;

	}else {
	   	return 1;
	}
}

void Read_SD_Sound(){
	int i = 0;
	short sound_buffer[51200];
	for ( i = 0; i < 10; i++ )
    {
	 TXBlockCounter = 0;
	 RXBlockCounter = 0;

	
	if ( MCI_Read_Block( i ) != TRUE )
		{
			while ( 1 );		/* Fatal error */
		}
		/* When RX_ACTIVE is not set, it indicates RX is done from 
		Interrupt_Read_Block, now, validation of RX and TX buffer can start. */
		while ( MCI_STATUS & MCI_RX_ACTIVE );
		/* Note RXEnable() is called in the Interrupt_Read_Block(). */
		MCI_RXDisable();
	for(i=0;i<5120;i++){
	    sound_buffer[i] = ReadBlock[i];
		//sound_buffer[i] += ReadBlock[i+1];    
	}
	PINSEL1 |= 0x200000;	
	while(1){
	for(i=0;i<51200;i++){
	  DACR = sound_buffer[i]; 
	}
	}

    }
}

void Read_SD(int number){
	int i = 0;
	for ( i = number; i > 0; i-- )
    {
	 TXBlockCounter = 0;
	 RXBlockCounter = 0;

	
	if ( MCI_Read_Block( i ) != TRUE )
		{
			while ( 1 );		/* Fatal error */
		}
		/* When RX_ACTIVE is not set, it indicates RX is done from 
		Interrupt_Read_Block, now, validation of RX and TX buffer can start. */
		while ( MCI_STATUS & MCI_RX_ACTIVE );
		/* Note RXEnable() is called in the Interrupt_Read_Block(). */
		MCI_RXDisable();
		LCDWritebmp((unsigned char *)ReadBlock,sizeof ReadBlock,0,0,131,131);
    }
}

