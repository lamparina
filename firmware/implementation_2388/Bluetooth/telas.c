#include <lpc23xx.h>
#include "type.h"
#include "irq.h"
#include "spi.h"
#include "lcd.h"
#include "figuras.h"
#include "telas.h"
#include "timer.h"
#include "ApplicationProtocol.h"
#include "MainApplication.h"
#include "SD.h"




short BUTTON1 = 0;
short BUTTON2 = 1;
short BUTTON3 = 2;
short BUTTON4 = 3;
short BUTTON5 = 4;
short BUTTON6 = 5;


int x_cont = 0;
int y_cont = 0;
int speed = 8000000;
char Actual_Button =0;
char stateButtons =0;

int x11 = 1;
int x22 = 38;
int x33 = 75;
int y1=0;
int y2 =91;

void TelaHome(void){
	
		LCDWritebmp((unsigned char *)msg_navegacao,tam_msg_navegacao,0,0,130,130);
		
		while(IMG_CODE == TELA_HOME){
		    Comunication();
			//Delay(100);
		}
		 
}

void TelaGenerica(void){
	
		LCDClearScreen(WHITE);
		LCDWritebmp((unsigned char *)fechar,tam_fechar,x11,y2,37,39);
		LCDWritebmp((unsigned char *)barra,tam_barra,112,0,21,130);
		
		while(IMG_CODE == TELA_GENERICA){
		    Comunication();
		    //Delay(100);
		}
		 
}

void MenuArquivos(void){
	    
		LCDClearScreen(WHITE);
		LCDWritebmp((unsigned char *)filtro_cd,tam_filtro_cd,x22,y1,37,39);
		LCDWritebmp((unsigned char *)filtro_doc,tam_filtro_doc,x11,y1,37,39);
		LCDWritebmp((unsigned char *)filtro_fotos,tam_filtro_fotos,x33,y1,37,39);
		LCDWritebmp((unsigned char *)fechar,tam_fechar,x11,y2,37,39);
		LCDWritebmp((unsigned char *)filtro_lista_sel,tam_filtro_lista_sel,x33,y2,37,39);       
		LCDWritebmp((unsigned char *)filtro_min,tam_filtro_min,x22,y2,37,39);
	   	LCDWritebmp((unsigned char *)multimidia,tam_multimidia,112,0,21,130);
		while(IMG_CODE == MENU_ARQ){

		  Comunication();

          if(Button_Pressed == 2){
		      if(((stateButtons>>BUTTON1)&0x01)){
			      LCDWritebmp((unsigned char *)filtro_cd,tam_filtro_cd,x22,y1,37,39); 
				  stateButtons &= (~(1<<BUTTON1)); 
			  }else {
			      LCDWritebmp((unsigned char *)filtro_cd_sel,tam_filtro_cd_sel,x22,y1,37,39); 
				  stateButtons |= (1<<BUTTON1);
			  }
		  }else if(Button_Pressed == 1){
			  if(((stateButtons>>BUTTON2)&0x01)){
			      LCDWritebmp((unsigned char *)filtro_doc,tam_filtro_doc,x11,y1,37,39); 
		  	      stateButtons &= (~(1<<BUTTON2));
			  }else {
			      LCDWritebmp((unsigned char *)filtro_doc_sel,tam_filtro_doc_sel,x11,y1,37,39);  
			      stateButtons |= (1<<BUTTON2);
			  }
		  }else if(Button_Pressed == 3){
		      if(((stateButtons>>BUTTON3)&0x01)){
			      LCDWritebmp((unsigned char *)filtro_fotos,tam_filtro_fotos,x33,y1,37,39);
			      stateButtons &= (~(1<<BUTTON3));
			  }else {
			      LCDWritebmp((unsigned char *)filtro_fotos_sel,tam_filtro_fotos_sel,x33,y1,37,39);
				  stateButtons |= (1<<BUTTON3);
			  }
		  }else if(Button_Pressed == 4){
                  LCDWritebmp((unsigned char *)fechar_sel,tam_fechar_sel,x11,y2,37,39);
		          Actual_Button =4;
				  LigaRelogio();
			      set_TIME(300,4);
		  }else if(Button_Pressed == 5){
		  	    	  Actual_Button = 5;
				   	  LCDWritebmp((unsigned char *)filtro_min_sel,tam_filtro_min_sel,x22,y2,37,39);
					  LCDWritebmp((unsigned char *)filtro_lista,tam_filtro_lista,x33,y2,37,39);        
				
		  }else if(Button_Pressed == 6){
		              Actual_Button = 6;
					  LCDWritebmp((unsigned char *)filtro_lista_sel,tam_filtro_lista_sel,x33,y2,37,39);
					  LCDWritebmp((unsigned char *)filtro_min,tam_filtro_min,x22,y2,37,39);       
				  
		  }
        		  
	    Button_Pressed =0;  

		}
		 
}

void Recentes(void){
	
		LCDClearScreen(WHITE);
		LCDWritebmp((unsigned char *)recentes_apagar,tam_recentes_apagar,x11,y1,37,39);
		LCDWritebmp((unsigned char *)recentes_pendrive,tam_recentes_pendrive,x22,y1,37,39);
		//LCDWritebmp((unsigned char *)botao_nulo,tam_botao_nulo,x33,y1,37,39);		
		LCDWritebmp((unsigned char *)fechar,tam_fechar,x11,y2,37,39);
		LCDWritebmp((unsigned char *)filtro_lista_sel,tam_filtro_lista_sel,x33,y1,37,39);       
		LCDWritebmp((unsigned char *)filtro_min,tam_filtro_min,x33,y2,37,39);
	   	LCDWritebmp((unsigned char *)barra,tam_barra,112,0,21,130);
		while(IMG_CODE == RECENTES){

		  Comunication();

		  if(get_TIME(4)==0){	
			if (Actual_Button==1)  LCDWritebmp((unsigned char *)recentes_apagar,tam_recentes_apagar,x11,y1,37,39);
			else if (Actual_Button==2)  LCDWritebmp((unsigned char *)recentes_pendrive,tam_recentes_pendrive,x22,y1,37,39);
			else if (Actual_Button==4)  LCDWritebmp((unsigned char *)fechar,tam_fechar,x11,y2,37,39);
		   	Actual_Button =0;
		  }	

          if(Button_Pressed == 1){
		      LCDWritebmp((unsigned char *)recentes_apagar_sel,tam_recentes_apagar_sel,x11,y1,37,39); 
			  Actual_Button = 1;
			  LigaRelogio();
			  set_TIME(300,4);
		  }else if(Button_Pressed == 2){
			  LCDWritebmp((unsigned char *)recentes_pendrive_sel,tam_recentes_pendrive_sel,x22,y1,37,39);
		      Actual_Button = 2;
			  LigaRelogio();
			  set_TIME(300,4);
		  }else if(Button_Pressed == 5){
		      
		  }else if(Button_Pressed == 4){
              LCDWritebmp((unsigned char *)fechar_sel,tam_fechar_sel,x11,y2,37,39);
		      Actual_Button =4;
			  LigaRelogio();
			  set_TIME(300,4);
		  }else if(Button_Pressed == 6){
		  	  Actual_Button = 6;
			  LCDWritebmp((unsigned char *)filtro_min_sel,tam_filtro_min_sel,x33,y2,37,39);
			  LCDWritebmp((unsigned char *)filtro_lista,tam_filtro_lista,x33,y1,37,39);        
		  }else if(Button_Pressed == 3){
		      Actual_Button = 3;
		      LCDWritebmp((unsigned char *)filtro_lista_sel,tam_filtro_lista_sel,x33,y1,37,39);
			  LCDWritebmp((unsigned char *)filtro_min,tam_filtro_min,x33,y2,37,39);       
		  }
       			  
	    Button_Pressed =0;  

		}
		 
}


void TelaFiguras(void){
	
	   LCDClearScreen(WHITE);
	   LCDWritebmp((unsigned char *)galeria_atualizar,tam_galeria_atualizar,x11,y1,37,39);
	   LCDWritebmp((unsigned char *)galeria_voltar,tam_galeria_voltar,x22,y1,37,39);
	   LCDWritebmp((unsigned char *)galeria_zoomIn,tam_galeria_zoomIn,x33,y1,37,39);
	   LCDWritebmp((unsigned char *)fechar,tam_fechar,x11,y2,37,39);
	   LCDWritebmp((unsigned char *)galeria_avancar,tam_galeria_avancar,x22,y2,37,39);
	   LCDWritebmp((unsigned char *)galeria_zoomOut,tam_galeria_zoomOut,x33,y2,37,39);       
	   LCDWritebmp((unsigned char *)multimidia,tam_multimidia,112,0,21,130);		
		
		while(IMG_CODE == TELA_FIGURAS){

		  Comunication();
          
		  //*if(Button_Pressed){
		  if(get_TIME(4)==0){	
			if (Actual_Button==1)  LCDWritebmp((unsigned char *)galeria_atualizar,tam_galeria_atualizar,x11,y1,37,39);
			else if (Actual_Button==2)  LCDWritebmp((unsigned char *)galeria_voltar,tam_galeria_voltar,x22,y1,37,39);
		  	else if (Actual_Button==3)  LCDWritebmp((unsigned char *)galeria_zoomIn,tam_galeria_zoomIn,x33,y1,37,39);
		  	else if (Actual_Button==4)  LCDWritebmp((unsigned char *)fechar,tam_fechar,x11,y2,37,39);
		  	else if (Actual_Button==5)  LCDWritebmp((unsigned char *)galeria_avancar,tam_galeria_avancar,x22,y2,37,39);
	      	else if (Actual_Button==6)  LCDWritebmp((unsigned char *)galeria_zoomOut,tam_galeria_zoomOut,x33,y2,37,39);       
			
	      	Actual_Button =0;
		  }		  

		  if(Button_Pressed == 1){
		      LCDWritebmp((unsigned char *)galeria_atualizar_sel,tam_galeria_atualizar_sel,x11,y1,37,39);
		      Actual_Button =1;
		  	  LigaRelogio();
			  set_TIME(300,4);
		  }else if(Button_Pressed == 2){
			  LCDWritebmp((unsigned char *)galeria_voltar_sel,tam_galeria_voltar_sel,x22,y1,37,39); 
		  	  Actual_Button =2;	
		  	  LigaRelogio();
			  set_TIME(300,4);
		  } else if(Button_Pressed == 3){
		      LCDWritebmp((unsigned char *)galeria_zoomIn_sel,tam_galeria_zoomIn_sel,x33,y1,37,39);
			  Actual_Button =3;
		      LigaRelogio();
			  set_TIME(300,4);
		  }else if(Button_Pressed == 4){
			 LCDWritebmp((unsigned char *)fechar_sel,tam_fechar_sel,x11,y2,37,39);
		     Actual_Button =4;
		     LigaRelogio();
			 set_TIME(300,4);
		  }else if(Button_Pressed == 5){
		  	 LCDWritebmp((unsigned char *)galeria_avancar_sel,tam_galeria_avancar_sel,x22,y2,37,39); 
		  	 Actual_Button =5;
		     LigaRelogio();
			 set_TIME(300,4);
		  }else if(Button_Pressed == 6){
		      LCDWritebmp((unsigned char *)galeria_zoomOut_sel,tam_galeria_zoomOut_sel,x33,y2,37,39);       
		      Actual_Button =6;
		      LigaRelogio();
			  set_TIME(300,4);
		  }
		  
	   Button_Pressed =0;  
	   }
		 
}



void Tela_msg_baseLigada(void){
LCDWritebmp((unsigned char *)msg_base_ligada,tam_msg_base_ligada,0,0,130,130);
}

void Tela_msg_Autenticando(void){
LCDWritebmp((unsigned char *)msg_autenticacao,tam_msg_autenticacao,0,0,130,130);
}
 
void telaInicial(void){

LCDWritebmp((unsigned char *)logo_inicial,tam_logo_inicial,0,0,131,131);
}

void Tela_msg_Conectando(void){

 LCDWritebmp((unsigned char *)msg_procurando_base,tam_msg_procurando_base,0,0,130,130);
}
