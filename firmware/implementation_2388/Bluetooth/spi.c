#include "LPC23xx.h"			
#include "spi.h"

void InitSSP0( void )
{
	 unsigned char i =0;
	 volatile unsigned int Dummy;

	 PCONP |= (1 << 21); //habilita SSP0

     PINSEL3 |= (P_SCK + P_MOSI + P_MISO); // Configura portas SPI
 	 // S0SPCR = BitEnable | CPHA | CPOL | MSTR | LSBF | SPIE | BITS; //Configura modo de funcionamento SPI
     // S0SPCCR = SPCLK; // Configura o Clock

 	 SSP0CR1 =0x00;    // Disable module // Disable Loop Back Mode	// Master mode
 
     SSP0CR0 &=0xFF0F; // SPI MODE // CPOL = 0 //CPHA =0
     SSP0CR0 |=0x0008; // 9 bits

     SSP0CR1 |= 0x02;  // enable module

	 SSP0IMSC = 0;

	 SSP0DMACR = 0;

     for (i = 0; i < 8; i++ )
     {
    	 Dummy = SSP0DR; // clear the RxFIFO
     }
  
 	 // Set SSP clock frequency
 	 SSP0CPSR = 12;

}
 
void sendSSP0(unsigned int data) 
{
	while(!(SSP0SR & 0x2));
    SSP0DR = data;
  	while(!(SSP0SR & 0x10));
  
    	
}
