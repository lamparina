#include "lpc23xx.h"
#include <stdio.h>
#include "type.h"
#include "uart.h"
#include "timer.h"
#include "irq.h"


//definicao das variaveis 
char mcSW_RxBuffer[RX_BUFFER_LENGHT];
BYTE bSW_NRXBytes;
BYTE bPtrSW_RxBufferRD;
BYTE bPtrSW_RxBufferWR;
BYTE bSW_Time_Out_Cmd;
//int qtd_byte_recebidos;
const CHAR RA_INT_TO_HEX[] = "0123456789ABCDEF";
int PACKET_SIZE = 33;
int PACKET_RECEIVED;
int DATA_MODE;
int INT_ENABLE;


 
BOOL InitUart(void){

	PINSEL0 |= 0x40000000;               /* Enable TxD1                       */
    PINSEL1 |= 0x00000001;               /* Enable RxD1                       */

	U1FDR    = 0xC1;	
	U1LCR = UART_EIGHT_BITS + UART_ONE_STOP_BITS + UART_DLAB; //0x03 + 0x00 + 0x80
	U1DLL = /*6;*/PCLK /(BAUD_RATE << 4); //0x4E;//6;//PCLK /(BAUD_RATE << 4); //0x4E;
	U1DLM = 0x00;
	U1LCR = UART_EIGHT_BITS + UART_ONE_STOP_BITS;
    U1FCR = UART_TRIGGER_LEVEL_1 + UART_RESET_TX_FIFO + UART_RESET_RX_FIFO + UART_FIFO_ENABLE;
	//0xC0 + 0x04 + 0x02 + 0x01 
	U1IER = UART_RX_INT_ENABLE;//habilita interrup��o da recep��o de dados
	if(install_irq(UART1_INT, (void *) Uart_RXInterrupt, UART_1_Priority )){
       return TRUE;
   }else {
	   return FALSE;
   } 	
}

void UART1_Clear(void){

U1FCR |= UART_RESET_RX_FIFO + UART_RESET_TX_FIFO;
//U1FCR = UART_TRIGGER_LEVEL_3;// + UART_RESET_TX_FIFO;
bPtrSW_RxBufferWR =0;
}

void UART1_Enable_Interurpt(void){

  
  //VICVectPriority7 = /*IRQ_SLOT_ENABLE +*/ UART_1;
  //VICVectAddr7 = (unsigned long) Uart_Interrupt;
  VICIntEnable |= INT_UART1_ENABLE;// INT_RTC_ENABLE;  
  U1IER = UART_RX_INT_ENABLE;  //0x01 UART_RX_INT_ENABLE;

}


void UART1_Disable_Interurpt(void){

  VICIntEnClr |= INT_UART1_ENABLE;  //INT_UART1_ENABLE;
  U1IER = 0x00;  //UART_RX_INT_ENABLE;

}

void fRA_SendHex(BYTE cDado){
	fRA_SendChar(fRA_NIBLE_H(cDado));
	fRA_SendChar(fRA_NIBLE_L(cDado));
}

/*********************** *****************************************************
**
**   Funcao:   fRA_SendChar()
**
**   Descricao: Fun��o que envia caracters ASCII pela serial
**
**   Parametros de Entrada: BYTE cDado          // Numero a ser convertido e enviado
**
**   Parametros de Saida: Nenhum
**
****************************************************************************/
void fRA_SendChar(CHAR cDado){
    
 char Dado;
 Dado= cDado; 
 
 while(TX1_FIFO_FULL());   // aguarda ate que haja espaco na fifo de transmissao
   	
 	U1THR = Dado;	   		//transmite dado
  
}

void fRA_SendShort(short pDado){
 char aux1 = pDado;
 char aux2 = (pDado>>8);   
 fRA_SendChar(aux1);
 fRA_SendChar(aux2);
  
}
/****************************************************************************
**
**   Funcao:  	fSW_GetCharRX()
**
**   Descricao: Funcao que poe o dado recebido pela serial no buffer passado como parametro
**
**   Parametros de Entrada: CHAR *mcBuffer - buffer que receber� um dado lido da serial						 
**
**   Parametros de Saida: BOOL indicando se h� algum dado no buffer de recepcao
**
****************************************************************************/
BOOL fSW_GetCharRX(CHAR *mcBuffer){

	if (bSW_NRXBytes >0){
		bSW_NRXBytes--;
		*mcBuffer = mcSW_RxBuffer[bPtrSW_RxBufferRD++];
		 if (bPtrSW_RxBufferRD == RX_BUFFER_LENGHT)
			bPtrSW_RxBufferRD = 0;
		return(TRUE);
	}
	else 
	  return(FALSE);   
}

int GetCharRX(CHAR mcBuffer[]){
	int indexb=bPtrSW_RxBufferWR;
	int index=0;
	while(indexb >0){
		indexb--;
		mcBuffer[index] = mcSW_RxBuffer[index];
		index++;	 
	}
	return index;
}
/****************************************************************************
**
**   Funcao:  	fSW_SendString()
**
**   Descricao: Funcao que envia strings pela serial
**
**   Parametros de Entrada: CHAR *mcBuffer - ponteiro para buffer de dados a serem enviados
**							unsigned short int  numberData - numero de bytes a serem enviados
**
**   Parametros de Saida: Nenhum
**
****************************************************************************/
void fSW_SendString(CHAR *mcBuffer, unsigned short int  numberData){
	
 char *buffer;
 buffer = mcBuffer;
 // while (*buffer){
 	while(numberData){
  	if(TX1_FIFO_EMPTY())   // se tem espaco na fifo
    {
	   U1THR = *buffer;		 // escreve dado
        buffer++;
		numberData--;
		       
    }
 }
}


void Uart_RXInterrupt(void)__irq{

  //IENABLE;
  //if(INT_ENABLE){
   if (RX_DATA_READY() ){	 // se recebeu dado
  	mcSW_RxBuffer[bPtrSW_RxBufferWR++]= U1RBR;  // le dado
    if (bPtrSW_RxBufferWR == RX_BUFFER_LENGHT){	  // guarda no buffer
  		bPtrSW_RxBufferWR = 0;
	}
	if(DATA_MODE){
	  if(bPtrSW_RxBufferWR == PACKET_SIZE){
	      UART1_Disable_Interurpt();
		  PACKET_RECEIVED = 1;
	  	  //INT_ENABLE = FALSE; 	
	  }
	}

  //}	
	//IDISABLE;
	VICVectAddr = 0;

 }
}
