#!/usr/bin/env python
#-*- coding: utf-8 -*-
#=========================
# Filename:  lamp_bluetooth_manager.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-09-1
#-------------------------
# Description:
#
#-------------------------
# License:
#
#=========================
import dbus
import dbus.glib
import dbus.service
import gobject
import sys
import re
import time
import subprocess

from lamp_utils.socket_server_rfcomm import *
from lamp_utils.lamp_image_handler import *

class BluetoothAdapter:
    def __init__(self, adapter_name="/org/bluez/hci0"):
        try:
            self._name = adapter_name
            self.__authenticated = False
            bus = dbus.SystemBus()
            bmgr = dbus.Interface(bus.get_object('org.bluez', '/org/bluez'), 'org.bluez.Manager')
            obj = bus.get_object('org.bluez', adapter_name)
            self.adapter = dbus.Interface(obj, 'org.bluez.Adapter')       
            #self._discovered_devices = {}
            self.adapter.SetMode("discoverable")
            self.adapter.SetName("lampejo")
            self.bluetooth_rfcomm_server = SocketServerRfcomm()
            self.protocol_action = {"0": self.bt_authetication,
                                    "1": lambda s:None,
                                    "2": self.bt_move_cursor,
                                    "3": self.bt_enter,
                                    "4": lambda s:None,
                                    "5": self.stop_request_image_control_change,#bt_change_control_image,
                                    "6": lambda s:None,
                                    "7": lambda s:None,
                                    "8": lambda s:None,
                                    "9": self.bt_get_image}
            self.bluetooth_rfcomm_server.set_polling_callback(self.decode_statment)
            self.image_control_changed = Event()
        except dbus.exceptions.DBusException, e:
            print "DEBUG: Bluetooth no work properly. Error: %s" % e
            
    def __repr__(self):
        return "Adapter: %s" % self._name
    
    def __eq__(self, adp_name):
        return self._name == adp_name
    
    def start_lampejo_rfcomm_server(self):
        if self.bluetooth_rfcomm_server:
            self.bluetooth_rfcomm_server.start_server()
        else:
            print "DEBUG: Error, bluetooth server does not exists!"
            
    def bt_load_data_package(self, stmt):
        length = int(stmt[1:5])
        if length > 0:
            return self.bluetooth_rfcomm_server.read(length)
        
    def bt_authetication(self, stmt):
        data = self.bt_load_data_package(stmt)
        if data == "#$%%$#":
            self.bluetooth_rfcomm_server.write("1%$##$%@@@@@@@@@@@@@@@@@@@@@@@@@@")
            
        elif len(data) == 16:
            self.bluetooth_rfcomm_server.write("0%s@@@@@@@@@@@@@@@@" % data)
            print "DEBUG: Lampejo RFCOMM Service authenticated!"
            self.__authenticated = True
        else:
            print "DEBUG: Lampejo RFCOMM Service invalid key!"
    
    def bt_move_cursor(self, stmt):
        data = self.bt_load_data_package(stmt)
        print "Command content:", data
        if data == "U":
            subprocess.call(["xvkbd","-text","\[Up]"])
            print "UP"
        elif data == "D":
            subprocess.call(["xvkbd","-text","\[Down]"])
            print "DOWN"
        elif data == "L":
            subprocess.call(["xvkbd","-text","\[Left]"])
            print "LEFT"
        elif data == "R":
            subprocess.call(["xvkbd","-text","\[Right]"])
            print "RIGHT"
        else:
            print "DEBUG: Lampejo RFCOMM Service moving action invalid!"
    
    def bt_enter(self, stmt):
        data = self.bt_load_data_package(stmt)
        if data == "OK":
            subprocess.call(["xvkbd","-text","\[Return]"])
            print "ENTER"
        elif data == "AT":
            print "Alt + tab"
            subprocess.call(["python","/home/lampejo/lamparina/src/lamp_utils/lampejo_core_remote.py","-s"])       
        elif data == "HO":
            print "Show home!"
            subprocess.call(["python","/home/lampejo/lamparina/src/lamp_utils/lampejo_core_remote.py","-i"])
        elif data == "B1":
            print "B1"
            subprocess.call(["xvkbd","-text","\C3"])
        elif data == "B2":
            print "B2"
            subprocess.call(["xvkbd","-text","\C2"])
        elif data == "B3":
            print "B3"
            subprocess.call(["xvkbd","-text","\C4"])
        elif data == "B4":
            print "B4"
            subprocess.call(["python","/home/lampejo/lamparina/src/lamp_utils/lampejo_core_remote.py","-c"])
        elif data == "B5":
            print "B5"
            subprocess.call(["xvkbd","-text","\C6"])
        elif data == "B6":
            print "B6"
            subprocess.call(["xvkbd","-text","\C5"])
        else:
            print "DEBUG: Lampejo RFCOMM Service enter action invalid!"
    
    @threaded
    def send_control_image(self, sent_data):
        while not self.image_control_changed.isSet():
            #sent_data = "5%02d@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" % code
            self.bluetooth_rfcomm_server.write(sent_data)
            time.sleep(1)
    
    def bt_change_control_image(self, code):#, stmt):
        #self.bluetooth_rfcomm_server.stop_polling_input_data()
        if self.__authenticated:
            code %= 100
            sent_data = "5%02d@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@" % code
            self.image_control_changed.set()
            self.image_control_changed.clear()
            self.send_control_image(sent_data)
        #self.bluetooth_rfcomm_server.write(sent_data)

    def stop_request_image_control_change(self, stmt):
        print "loading data by %s packet command" % stmt
        print "Current screen:", self.bt_load_data_package(stmt)
        self.image_control_changed.set()
    
    def bt_get_image(self, stmt):
        res = []
        data = self.bt_load_data_package(stmt)
        if re.match(r"^#\$%\d+%\$#$",data):
            len_image = 0
            try:
                len_image = int(re.findall(r"\d+", data)[0])
                print "Numero de pacotes a receber:", len_image
            except IndexError:
                raise Exception("Image data length is malformed")
            sent_data = ("9%s"% data) + "@"*(33-len(data)+1)
            print "DEBUG: confirmacao da imagem: %s" % sent_data 
            #self.bluetooth_rfcomm_server.stop_polling_input_data()            
            self.bluetooth_rfcomm_server.write(sent_data)
            count = 0 
            while count < len_image: 
                stmt = self.bluetooth_rfcomm_server.read()
                print "imagemmm", stmt, len(stmt)
                if stmt == "90009":
                    print "DEBUG: Discarted...", self.bt_load_data_package(stmt)
                else:
                    count+=1
                    rc_data = self.bt_load_data_package(stmt)
                    print "Imagem recebida...%d:" % count, rc_data, len(rc_data)
                    res.append(rc_data)
            raw_image = "".join(res)
            rgb_image = translate_from_rgb3bytes(raw_image)
            build_picture(100, 100, rgb_image, "/tmp/velha_mini.png")
            f = open("/tmp/test_bt_get_image", "w+")
            f.write(raw_image)
            f.close()
            #self.bluetooth_rfcomm_server.restart_polling_input_data()
            
    
    def decode_statment(self, stmt):
        try:
            assert len(stmt) == 5
            print "DEBUG: Remote control comand:", stmt
            cmd = stmt[0]
            self.protocol_action[cmd](stmt)
        except:
            print "DEBUG: Bluetooth package discarded!"
            #print "Exception: ", sys.exc_info()
    
    def shutdown(self):
        self.bluetooth_rfcomm_server.shutdown_server()
            

class LampBluetoothManager:
    def __init__(self):
        print "Startup the Lampejo bluetooth manager daemon...ok"
        bus = dbus.SystemBus()
        obj = bus.get_object('org.bluez', '/org/bluez')
        self._bmanager = dbus.Interface(obj, 'org.bluez.Manager')
        self.default_adapter = None
        self.adapters = {}
        self.load_adapters()
        
    def load_adapters(self):
        try:
            for ad in self._bmanager.ListAdapters() or []:
                self.adapters[ad] = BluetoothAdapter(str(ad))
            #print "Found adapters:", self.adapters
            self.default_adapter = self.get_default_adapter()
        except dbus.exceptions.DBusException, e:
            print "DEBUG: Bluetooth no work properly. Error: %s" % e
            

    def start_service(self):
        if self.default_adapter:
            self.default_adapter.start_lampejo_rfcomm_server()
        else:
            print "DEBUG: Bluetooth RFCOMM cannot be started!!!"
    
    
    def shutdown_service(self):
        print "Shutting down the Lampejo bluetooth manager daemon...",
        self.default_adapter and self.default_adapter.shutdown()
        print "ok"
        
    def get_default_adapter(self):
        return self.adapters[self._bmanager.DefaultAdapter()]
    
    def change_control_image(self, code):
        if self.default_adapter:
            self.default_adapter.bt_change_control_image(code)
        else:
            print "DEBUG: There ins't an adapter, the control image fail:", code

    @threaded
    def test_send_change_image(self):
        while 1:
            print "try to change image"
            raw_input("Change image\n")
            test.default_adapter.bt_change_control_image(21)
    
if __name__ == "__main__":
    try:
        test = LampBluetoothManager()
        test.test_send_change_image()
        test.start_service()
        gobject.threads_init()
        dbus.glib.init_threads()
        main_loop = gobject.MainLoop()
        main_loop.run()
    except KeyboardInterrupt:
        test.shutdown_service()
