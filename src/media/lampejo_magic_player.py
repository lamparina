#!/usr/bin/env python

import gobject
import gtk
import gtk.glade
import pygst
import pygtk
import sys, os

pygst.require("0.10")
import gst

#TODO: To implements video with gst-launch v4l2src ! xvimagesink

class GTK_Main:

    def __init__(self, file):
        glade_file = gtk.glade.XML("image/lamplayer.glade")
        self.main_win = glade_file.get_widget("main_win")
        self.movie_window = glade_file.get_widget("main_da")

        self.b_play = glade_file.get_widget("b_play")
        #self.b_pause = glade_file.get_widget("b_pause")
        self.b_forward = glade_file.get_widget("b_forward")
        self.b_rewind = glade_file.get_widget("b_rewind")

        self.main_win.connect("destroy", gtk.main_quit)
        self.main_win.show_all()
        self.main_win.fullscreen()

        glade_file.signal_autoconnect(self)

        self.player = gst.element_factory_make("playbin", "player")
        bus = self.player.get_bus()
        bus.add_signal_watch()
        bus.enable_sync_message_emission()
        bus.connect("message", self.on_message)
        bus.connect("sync-message::element", self.on_sync_message)
        
        self.player.set_property("uri", "file://" + file)
        #self.player.set_state(gst.STATE_READY)

    def on_b_play_clicked(self, *args):
        if self.b_play.get_label() == "gtk-media-play":
            #self.player.set_property("uri", "file:///home/rodrigopex/Projects/lampejo/src/media/media_test/test_picture.jpg")
            self.player.set_state(gst.STATE_PLAYING)
            self.b_play.set_label("gtk-media-pause")
        else:
            self.player.set_state(gst.STATE_PAUSED)
            self.b_play.set_label("gtk-media-play")

    def on_b_forward_pressed(self, *args):
        print "for press"
        p, d = self.get_position()
        step = d/500
        self.seek(p + step)

    def on_b_rewind_pressed(self, *args):
        print "rew press"
        p, d = self.get_position()
        step = d/500
        val = p - step
        if val > 0:
            self.seek(val)
        else:
            self.seek(0)


    def seek(self, location):
                "Move para localizacao temporal dentro do stream."
                event = gst.event_new_seek(1.0, gst.FORMAT_TIME,
                                gst.SEEK_FLAG_FLUSH | gst.SEEK_FLAG_ACCURATE,
                                gst.SEEK_TYPE_SET, location, gst.SEEK_TYPE_NONE, 0)

                res = self.player.send_event(event)
                if res:
                        gst.info("setting new stream time to 0")
                        self.player.set_new_stream_time(0L)
                else:
                        gst.error("seek to %r failed" % location)

    def get_position(self):
            "Returns a (position, duration) tuple"
            try:
                    position, format = self.player.query_position(gst.FORMAT_TIME)
            except:
                    position = gst.CLOCK_TIME_NONE
            try:
                    duration, format = self.player.query_duration(gst.FORMAT_TIME)
            except:
                    duration = gst.CLOCK_TIME_NONE

            return (position, duration)



    def start_stop(self, w):
        if self.b_play.get_label() == "gtk-media-play":
            filepath = self.entry.get_text()
            if os.path.isfile(filepath):
                self.b_play.set_label("gtk-media-pause")
                self.player.set_property("uri", "file://" + filepath)
                self.player.set_state(gst.STATE_PLAYING)
            else:
                print "file not found!"
        else:
            self.player.set_state(gst.STATE_NULL)
            self.b_play.set_label("gtk-media-play")

    def on_message(self, bus, message):
        t = message.type
        if t == gst.MESSAGE_EOS:
            self.player.set_state(gst.STATE_NULL)
            self.b_play.set_label("gtk-media-play")
        elif t == gst.MESSAGE_ERROR:
            self.player.set_state(gst.STATE_NULL)
            err, debug = message.parse_error()
            print "Error: %s" % err, debug

    def on_sync_message(self, bus, message):
        if message.structure is None:
            return
        message_name = message.structure.get_name()
        if message_name == "prepare-xwindow-id":
            imagesink = message.src
            imagesink.set_property("force-aspect-ratio", True)
            imagesink.set_xwindow_id(self.movie_window.window.xid)

if __name__ == '__main__':
    import sys
    sys.argv[1]
    GTK_Main()
    gtk.gdk.threads_init()
    gtk.gdk.threads_enter()
    gtk.main()
    gtk.gdk.threads_leave()
