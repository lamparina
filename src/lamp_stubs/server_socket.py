#!/usr/bin/env python
#-*- coding: utf-8 -*-

#=========================
# Filename:  server_socket.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-08-20
#--------------------------------------------------
# Description:
# It's responsible for the application management.
#--------------------------------------------------
# License:
#
#==================================================

import os
import sys
import re
import socket
import time
from threading import Thread
print os.getcwd()
sys.path.append(os.path.join(os.getcwd(),".."))
from lamp_application_manager import *

HOST = ''                 # Symbolic name meaning the local host
PORT = 50007              # Arbitrary non-privileged port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(5)
conns_ = []

gamb = LampApplicationManager("../../config/lampejo.cfg")

def threaded(func):
    def proxy(*args, **kwargs):
        thr = Thread(target=func, args=args, kwargs=kwargs)
        thr.start()
        return thr
    return proxy

@threaded
def server_start():
    while 1:
        conn, addr = s.accept()
        conns_.append((conn, addr))
        print 'Connected by', addr
        print len(conns_)
        #if len(conns_) == 1:
        #    replection_data(conn)
        #    send_data(conn,"YOU-ARE-THE-MASTER")

@threaded
def reflection():
    while 1:
        conns_ or time.sleep(0.5)
        for con in conns_:
            data = unicode(con[0].recv(64), "UTF-8").strip()
            #Verify if data is exists, and if data is valid if both true 
            #data will be the same else data will be None
            data = data and \
                        re.match(r"^#(exec|open) [a-zA-Z0-9 _-]+#$", data) and \
                        data[1:-1] or \
                        None
            #Verify if data is True and then run the application
            data and gamb.run_application(data.strip().split()[1])
            
def notify_all():
    for c in conns_:
        try:
            c[0].send("NOTIFY")
        except socket.error:
            conns_.remove(c)

def killall():

    for c in conns_:
        try:
            c[0].send("KILL-IT-SELF")
            c[0].close()
        except socket.error:
            pass
        conns_.remove(c)
    print conns_

@threaded
def send_data(con,pkt):
    try:
        con.send(pkt)
    except socket.error:
        pass

@threaded
def send_data_broadcast(pkt):
    for c in conns_:
        try:
            c[0].send(pkt)
        except socket.error:
            pass
    
if __name__=="__main__":
    server_start()
    reflection()
    #time.sleep(8)
    #send_data_broadcast("block:10.1.1.17")
    #replection_data()
    #for i in range(5):
    #    time.sleep(3)
    #    notify_all()
    raw_input("Finalizar servidor agora? [Enter]")
    #send_data("teste de evento")
    time.sleep(1)
        
    #while conns_:
    #    killall()
    gamb.kill_current_application()
    gamb.kill_current_application()
    gamb.kill_current_application()
    s.close()
    #print "Close server"
    sys.exit(0)
