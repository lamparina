#!/usr/bin/env python
#-*- coding: utf-8 -*-

#=========================
# Filename:  flash_stub.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-08-20
#--------------------------------------------------
# Description:
# It's responsible for the application management.
#--------------------------------------------------
# License:
#
#==================================================

from threading import Thread
import gtk
import gtk.glade
import os
import sys
import subprocess

global LAMPEJO_CORE_PATH
LAMPEJO_CORE_PATH=os.getenv("LAMPEJO_CORE_PATH")
if LAMPEJO_CORE_PATH: 
    sys.path.append(LAMPEJO_CORE_PATH)
else:
    raise SystemExit("Error, the environment variable LAMPEJO_CORE_PATH is not defined.")


from lamp_utils.socket_client import SocketClient
from lamp_utils.socket_client import SocketConnectionError

def threaded(func):
    def proxy(*args, **kwargs):
        thr = Thread(target=func, args=args, kwargs=kwargs)
        thr.start()
        return thr
    return proxy

@threaded
def show_message_dialog(message):
    dialog = gtk.MessageDialog(None, gtk.DIALOG_MODAL, gtk.MESSAGE_ERROR, gtk.BUTTONS_NONE, message)
    dialog.add_button(gtk.STOCK_CLOSE, gtk.RESPONSE_CLOSE)
    dialog.run()
    dialog.destroy()

class FlashStub:
    def __init__(self):
        gladefile = gtk.glade.XML(os.path.join(LAMPEJO_CORE_PATH,"lamp_stubs","flash_stub.glade"))
        self.main_win = gladefile.get_widget("mainw")
        self.main_win.connect("destroy", gtk.main_quit)
        gladefile.signal_autoconnect(self)
        #self.main_win.fullscreen()
        self.channel = None
        try:
            self.channel = SocketClient()
            self.channel and self.channel.polling_input_data(self.show_error)
        except SocketConnectionError:
            message = "Connection Error: The connection cannot be established."        
            show_message_dialog(message)

        
    def show_error(self, message):
        '''This is temporary!!! It depends the zenity application.
        '''
        show_message_dialog("From flash stub: " + message)
        sys.stderr.write("From flash stub: " + message)

    def on_firefox_clicked(self, *args):
        print "firefox"
        self.channel and self.channel.write_channel(u"#exec firefox#")
        
    def on_ooffice_clicked(self, *args):
        print "ooffice"
        self.channel and self.channel.write_channel(u"#exec ooffice#")
        
    def on_pendrive_clicked(self, *args):
        print "pendrive"
        self.channel and self.channel.write_channel(u"#exec xcalc#")
        
    def on_error_test_clicked(self, *args):
        print "error!"
        
    def run(self):
        self.main_win.show()
        gtk.main()
    
    def shutdonw(self):
        self.channel and self.channel.write(u"closing connection...") and self.channel.close()
        gtk.main_quit()
    
if __name__ == '__main__':
    test = FlashStub()
    test.run()
