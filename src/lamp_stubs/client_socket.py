#!/usr/bin/env python
#-*- coding: utf-8 -*-

#=========================
# Filename:  client_socket.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-08-20
#--------------------------------------------------
# Description:
# It's responsible for the application management.
#--------------------------------------------------
# License:
#
#==================================================

import socket
import sys
import subprocess
from threading import Thread
 
HOST = 'localhost'    # The remote host
PORT = 50007 # The same port as used by the server


def threaded(func):
    def proxy(*args, **kwargs):
        thr = Thread(target=func, args=args, kwargs=kwargs)
        thr.start()
        return thr
    return proxy

class DataCannotBeSentError(Exception): pass

class SocketConnectionError(Exception): pass

class ClientSocket:
    def __init__(self):
        self.skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.skt.connect((HOST, PORT))
        except socket.error:
            raise SocketConnectionError("The connection cannot be established. Verify the server.")
        
    @threaded
    def write(self, data):
        try:
            self.skt.send(data)
        except socket.error:
            raise DataCannotBeSentError("Socket Error, the data can not be sent.")
    
    @threaded
    def read(self):
        while 1:
            try:
                data = self.skt.recv(64)
                #yield data
                return data
            except socket.error:
                pass
