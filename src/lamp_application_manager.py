#!/usr/bin/env python
#-*- coding: utf-8 -*-
#=========================
# Filename:  lamp_application_manager.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-08-08
#--------------------------------------------------
# Description:
# It's responsible for the application management.
#--------------------------------------------------
# License:
#
#==================================================
from lamp_application_manager_util import *
from lamp_utils.lamp_built_in_functions import *
from lamp_utils.lamp_dialog_open import *
import lamp_application_switcher as las
import os
import types
import time

las.required("wmctrl")

STACK_DEPTH=6 #Length of the __execution_queue
global LAMPEJO_CORE_PATH
LAMPEJO_CORE_PATH=os.getenv("LAMPEJO_CORE_PATH")
if LAMPEJO_CORE_PATH: 
    sys.path.append(LAMPEJO_CORE_PATH)
else:
    raise SystemExit("Error, the environment variable LAMPEJO_CORE_PATH is not defined.")

class LampApplicationNotFoundError(Exception):
    '''Raised when an application is not found into the
    __avaliable_application
    '''
    pass

class LampExecutionQueueIsFullError(Exception):
    '''Raised when the app_manager.__execution_queue is full
    the STACK_DEPTH is the length of the queue
    '''
    pass

class LampExecutionQueueIsEmptyError(Exception):
    '''Raised when the app_manager.__execution_queue is empty
    '''
    pass

class LampApplicationManager(object):
    '''
    Tests:
        nop
    '''
    def __init__(self, path_to_cfg_file=os.path.join(LAMPEJO_CORE_PATH, 'config', 'lampejo.cfg'), control_code_callback=None):
        print "Startup the Lampejo application manager daemon...ok"
        loader = LoadConfigApplications(path_to_cfg_file)
        self.__avaliable_application = loader.get_available_applications()
        self.__execution_queue = []#(list)
        self._current_application = -1 #(int)
        self.__home_active = True
        self._home_application = self.get_available_app_by_alias(u"home")
        self._load_application = self.get_available_app_by_alias(u"load")
        self.__update_control_image_callback = None
        self._load_application.execute_itself()
        self._home_application.execute_itself()
        control_code_callback and self.set__update_control_image_callback(control_code_callback)
    
    def shutdown(self):
        self._home_application.kill_itself()
        for i in self.__execution_queue:
            i.kill_itself()
        print "Shutting down the Lampejo application manager daemon...ok"

    def get_available_app_by_alias(self, app_alias):
        '''
        Arguments:
            app_alias --
        
        Return:
            - An LampApplication instance that was found in the 
                self.__avaliable_application that represents a real application
                
        Exceptions raised:
            - TypeError when the app_alias is not a unicode string
        
        Tests:
        >>> test = LampApplicationManager("../config/test_app_manager.cfg")
        >>> tmp = test.get_available_app_by_alias(u"firefox")
        >>> if tmp: print tmp._app_alias
        firefox
        >>> tmp = test.get_available_app_by_alias(u"eog")
        >>> if tmp: print tmp._app_alias
        eog
        '''
        ret = None
        try:
            assert check_types({app_alias: unicode}), "Type Error! app_alias is not a unicode string"
            for app in self.__avaliable_application:
                if app._app_alias == app_alias:
                    ret = app
                    break
        except AssertionError:
            raise
        
        return ret
        
            
    def run_application(self, app_alias, file_source=None):
        '''Responsible for running applications based on app_alias
        
        Arguments:
            app_alias -- name of application which will be executed. It must
                exists an application into the self.__available_application
                list with the same alias of the app_alias.
                Ex.: firefox
            file_source -- is the file that will be opened. If it uses the
                run_applicaion method to open a file this argument must not be
                none.
                Ex.: soffice /home/lampejo/test/presentation.odp
                
        Exceptions raised:
            - LampApplicationNotFoundError when there is not an 
            LampApplication with the app_alias into the 
            self.__avaliable_application
            - AssertionError "Type Error! app_alias is not a unicode string" when the
            app_alias value is not a string
        
        Tests:
        >>> import time
        >>> test = LampApplicationManager("../config/test_app_manager.cfg")
        >>> tmp = test.get_available_app_by_alias(u"home")
        >>> print type(tmp._app_alias)
        <type 'unicode'>
        >>> test.run_application(tmp._app_alias)
        >>> test.run_application(tmp._app_alias)
        >>> tmp2 = test.get_available_app_by_alias(u"eog")
        >>> test.run_application(tmp2._app_alias, "../media_test/test_picture.jpg")
        >>> test.run_application(tmp2._app_alias)
        >>> tmp3 = test.get_available_app_by_alias(u"firefox")
        >>> test.run_application(tmp3._app_alias)
        >>> time.sleep(1)
        >>> test.run_application(tmp3._app_alias)
        >>> time.sleep(2)
        >>> test.switch_application()
        >>> time.sleep(2)
        >>> test.switch_application()
        >>> time.sleep(2)
        >>> test.switch_application()
        >>> time.sleep(2)
        >>> test.switch_application()
        >>> time.sleep(2)
        >>> test.kill_current_application()
        >>> test.kill_current_application()
        >>> test.kill_current_application()
        >>> test.show_home()
        '''
        print "DEBUG: trying to run application ", app_alias, file_source
        try:
            assert check_types({app_alias: unicode}), "Type Error! app_alias is not a unicode string"
            app = self.get_available_app_by_alias(app_alias)
            assert app, "Application %s is not available, fix it and try again" % app_alias
            self.show_loading()
            if file_source: 
                app._file_source = file_source
            if app in self.__execution_queue:
                if app.is_replaceable():
                    app.kill_itself()
                    app.execute_itself()
                    #subprocess.call(["wmctrl","-r", app._window_title, "-b", "add,fullscreen"])
                    #time.sleep(5)
                    las.alternate_application(app._window_title)
                else:
                    self.update_control_image(app)
                    las.alternate_application(app._window_title)
            elif len(self.__execution_queue) < STACK_DEPTH:
                self._current_application = (self._current_application + 1) % STACK_DEPTH
                app.execute_itself()
                self.update_control_image(app)
                self.__execution_queue.append(app)
                #time.sleep(5)
                las.alternate_application(app._window_title)
                #subprocess.call(["wmctrl","-r", app._window_title, "-b", "add,fullscreen"])
            else:
                raise LampExecutionQueueIsFullError("The execution queue is full!\nClose a application and try again.")
            app._file_source = None
        except AssertionError, e:
            print "DEBUG: Error -> %s" % e
        
    def switch_application(self, direction=False):
        '''Responsible for switching running applications.
        
        Arguments:
            direction -- defines the switching direction. False value  
            switches to lastest opened application. True switches to firstest
            opened application.
        
        Tests:
            nop
        '''
        step = direction and 1 or -1
        #self.clean_execute_queue()
        length_eq = len(self.__execution_queue)
        try:
            assert length_eq
            if self._current_application:
                self._current_application = (self._current_application + step) % length_eq
            else:
                self._current_application = -1
            next = self.__execution_queue[self._current_application]
            #if next._process.poll()!=None:
            #    self.__execution_queue.remove(next)
            #else:
            self.update_control_image(next)
            
            las.alternate_application(next._window_title)
                
        except IndexError, e:
            print e
        except AssertionError: #Ignored
            pass 
            
    def clean_execute_queue(self):
        for app in self.__execution_queue:
            if app._process.poll()!=None:
                self.__execution_queue.remove(app)
            
        
    def kill_current_application(self):
        '''Responsible for killing the application into the executing queue
        
        Tests:
            nop
        '''
        length_eq = len(self.__execution_queue)
        if length_eq:
            try:
                app = self.__execution_queue[self._current_application % length_eq]
                app.kill_itself()
                self.switch_application(True)
                self.__execution_queue.remove(app)
                if len(self.__execution_queue) == 0:
                    self.update_control_image(self._home_application)
                    self.show_home()
            except IndexError, e:
                print e
        else:
            raise LampExecutionQueueIsEmptyError("The execution queue is empty!")

    def show_home(self):
        '''Put the home application on focus.
        
        Tests:
            nop
        '''
        self.update_control_image(self._home_application)
        las.alternate_application(self._home_application._window_title)
    
    def show_loading(self):
        las.alternate_application(self._load_application._window_title)
    
    def hide_loading(self):
        try:
            self._load_application.kill_itself()
        except:pass

    
    def set__update_control_image_callback(self, callback):
        check_types({callback: types.MethodType})
        self.__update_control_image_callback = callback
    
    def update_control_image(self, app):
        if self.__update_control_image_callback:
            print "DEBUG: Control change with bluetooth to:", app._remote_control_code
            self.__update_control_image_callback(app._remote_control_code)
        else:
            print "DEBUG: Control change without bluetooth to:", app._remote_control_code
         

def _test():
    import doctest
    doctest.testmod()

if __name__ == '__main__':
    _test()


