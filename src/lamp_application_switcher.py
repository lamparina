#!/usr/bin/env python
#-*- coding: utf-8 -*-

#=========================================
# Filename:  lamp_application_switcher.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-08-18
#--------------------------------------------------
# Description:
# It's responsible for the application management.
#--------------------------------------------------
# License:
#
#==================================================

import subprocess
import sys
from lamp_utils.lamp_built_in_functions import check_types


COMMAND = "wmctrl"

def required(cmd=COMMAND):
    '''
    
    Tests:
    >>> required("wmctrl")
    True
    '''
    ret = False
    try:
        p = subprocess.Popen(cmd.strip().split(), stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        error = p.stderr.read()
        ret = True     
    except OSError, e:
        sys.stderr.write("The command %s is required to run this application.\nInstall this to fix it!" % cmd)
    return ret

def pywmctrl(*args):
    '''This method respresents the wmctrl command.
    
    Arguments:
        args -- should be a list with the options to wmctr command and for end the application name
            Ex.: pywmctrl("-c", "firefox") #to close the firefox window
            
    Exceptions raised:
        - AssertionError in the case of args will not a list
    
    Tests:
        found in alternate_application and close_application_gracefully functions
    '''
    try:
        assert check_types({args: tuple}), "Type Error! app is not a unicode string"
        p = subprocess.Popen(["wmctrl"] + list(args), stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    except AssertionError:
        raise

def alternate_application(app=u""):
    '''
    
    Tests:
    >>> import os
    >>> import time
    >>> required("wmctrl")
    True
    >>> p1 = subprocess.Popen("xcalc")
    >>> p2 = subprocess.Popen("xeyes")
    >>> time.sleep(2)
    >>> alternate_application(u"xcalc")
    >>> time.sleep(2)
    >>> alternate_application(u"xeyes")
    >>> os.kill(p1.pid, 9)
    >>> os.kill(p2.pid, 9)
    >>> p1.poll() == None
    False
    >>> p2.poll() == None
    False
    '''
    pywmctrl("-a", app)
  
def close_application_gracefully(app=u""):
    '''
    
    Tests:
    >>> import os
    >>> import time
    >>> required("wmctrl")
    True
    >>> p1 = subprocess.Popen(u"xcalc")
    >>> time.sleep(1)
    >>> p2 = subprocess.Popen(u"xeyes")
    >>> time.sleep(2)
    >>> close_application_gracefully(u"Calculator")
    >>> time.sleep(1)
    >>> close_application_gracefully(u"xeyes")
    >>> p1.poll() == None
    False
    >>> p2.poll() == None
    False
    '''
    pywmctrl("-a", app)
    pywmctrl("-c", app)


def _test():
    import doctest
    doctest.testmod()

if __name__ == '__main__':
    _test()    