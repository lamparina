#!/usr/bin/env python
#-*- coding: utf-8 -*-
#=========================
# Filename:  lamp_hardware_manager.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-08-08
#-------------------------
# Description:
# this modules is responsible for the
#-------------------------
# License:
#
#=========================

from lamp_utils.lamp_built_in_functions import check_types
import dbus
import dbus.glib
import gobject
import time


class VolumeAlreadyMountedError(Exception): pass
class VolumeIsNotMountedError(Exception): pass

class LampDevice(object):
    def __init__(self, dev_uid, dev_iface, vol_iface,):
        self.__dbus_device_interface = dev_iface
        self.__dbus_volume_interface = vol_iface
        self._dev_uid = dev_uid
        self._i_am = self.who_am_i()
        
    def __repr__(self):
        return "Device: %s" % self._dev_uid
    
    def __eq__(self, dev):
        return self._dev_uid == dev
    
    def mount(self, mount_point=None, fstype=None, options=None):
        ret = 1
        options = options or []
        if self._i_am == "storage":
            mount_point = "pendrive"
        elif self._i_am == "dvdcd":
            mount_point = "dvdcd"
        else:
            mount_point = mount_point or self.__dbus_device_interface.GetProperty("volume.label")
        check_types({mount_point: str, fstype: str})
        if self.is_mounted():
            raise VolumeAlreadyMountedError("Error: The volume is already mounted at %s" % str(self.mount_point()))
        else:
            print "DEBUG: Trying to mount at %s" % mount_point
            if fstype:
                ret = int(self.__dbus_volume_interface.Mount(mount_point, fstype, options))
            else:
                fstype = self.__dbus_device_interface.GetProperty("volume.fstype")
                ret = int(self.__dbus_volume_interface.Mount(mount_point, fstype, options))
        return ret
        
    def unmount(self, options=None):
        ret = 1
        options = options or []
        if self.is_mounted():
            ret = int(self.__dbus_volume_interface.Unmount(options))
        else:
            raise VolumeIsNotMountedError("Error: The volume is not mounted now.")
        return ret
    
    def eject(self, options=None):
        ret = 1
        options = options or []
        if self.is_mounted():
            ret = int(self.__dbus_volume_interface.Eject(options))
        else:
            raise VolumeIsNotMountedError("Error: The volume is not mount now.")
        return ret
    
    def mount_point(self):
        ret = str(self.__dbus_device_interface.GetProperty("volume.mount_point"))
        if not ret:
            print "DEBUG: not mounted yet trying to mount..."
            if self._i_am == "storage":
                self.mount(mount_point=None, fstype="vfat", options=["umask=000","iocharset=utf8"])
            else:
                #self.mount(mount_point=None, fstype="iso9660", options=["iocharset=utf8"])
                #TODO: Ajustar o tipo do dvdcd
                self.mount(mount_point=None, fstype=None, options=[])#options=["iocharset=utf8"])
            ret = str(self.__dbus_device_interface.GetProperty("volume.mount_point"))
        return ret
    
    def is_mounted(self):
        return bool(self.__dbus_device_interface.GetProperty("volume.is_mounted"))
    
    def who_am_i(self):
        ret = None
        bus = dbus.SystemBus()
        parent = self.__dbus_device_interface.GetProperty("info.parent")
        dev_par = bus.get_object ('org.freedesktop.Hal', parent)
        dev_par_iface = dbus.Interface (dev_par, 'org.freedesktop.Hal.Device')
        if dev_par_iface.GetProperty("storage.removable"):
            if "storage.cdrom" in dev_par_iface.GetProperty("info.capabilities"):
                    ret = "dvdcd"
            elif dev_par_iface.GetProperty("storage.bus") == "usb":
                ret = "storage"
            elif dev_par_iface.GetProperty("storage.bus") == "scsi":
                if "storage.cdrom" in dev_par_iface.GetProperty("info.capabilities"):
                    ret = "dvdcd"
            '''if dev_par_iface.GetProperty("storage.bus") == "usb":
                ret = "storage"
            elif dev_par_iface.GetProperty("storage.bus") == "scsi":
                if "storage.cdrom" in dev_par_iface.GetProperty("info.capabilities"):
                    ret = "dvdcd" '''
        return ret


class LampHardwareManager(object):
    def __init__(self):
        print "Startup the Lampejo hardware manager daemon...ok"
        self.bus = dbus.SystemBus()
        obj = self.bus.get_object('org.freedesktop.Hal', '/org/freedesktop/Hal/Manager')
        self.hal_manager = dbus.Interface(obj, 'org.freedesktop.Hal.Manager')
        
        self.hal_manager.connect_to_signal('DeviceAdded', self.hardware_detected_interrupt)
        self.hal_manager.connect_to_signal('DeviceRemoved', self.hardware_removed_interrupt)
        self.devices = []
        self.__hardware_insertion_notify_callback = None
    
    def set_hardware_insertion_notify_callback(self, callback):
        if callback:
            self.__hardware_insertion_notify_callback = callback
            
    def get_device_by_uid(self, uid):
        ret = None
        try:
            i = self.devices.index(uid)
            ret = self.devices[i]
        except ValueError:
            pass
        return ret
    
    def get_storage_device(self):
        ret = None
        for dev in self.devices:
            if dev._i_am == "storage":
                ret = dev
                break
        return ret
            
    
    def hardware_modify_notification(self, added=True, device=None, type=None):
        print "Hardware modified notification..."
        packet = None #"#No show devices#"
        try:
            dev = device or self.devices[-1]
            if added:
                packet = "#connected %(type)s %(mount_point)s#" % {"type":dev.who_am_i(),
                                                        "mount_point": dev.mount_point()}
            else:
                packet = type and "#removed %s#" % type
        except IndexError:
            pass
        if self.__hardware_insertion_notify_callback:
            packet and self.__hardware_insertion_notify_callback(packet)
        else:
            raise Exception("__hardware_insertion_notify_callback not valid!")
    
    def search_ready_devices(self):
        devices = self.hal_manager.GetAllDevices()
        for dev in devices:
            self.hardware_detected_interrupt(dev)
    
    def show_current_devices_stack(self):
        print "= Current devices stack ======"
        for dev in self.devices:
            print dev, dev.who_am_i()
        print "=============================="
            

    def hardware_detected_interrupt(self, device):
        dev_obj = self.bus.get_object ('org.freedesktop.Hal', device)
        dev_iface = dbus.Interface (dev_obj, 'org.freedesktop.Hal.Device')
        msg = None
        #print "detected device %s..." % device
        try:
            cap = dev_iface.GetProperty("info.capabilities")
            if "volume" in cap:
                vol_iface = dbus.Interface (dev_obj, 'org.freedesktop.Hal.Device.Volume')
                dev = LampDevice(device, dev_iface, vol_iface)
                if dev.who_am_i():
                    self.devices.append(dev)
                    self.hardware_modify_notification()
                    msg = "= Device added...OK -> %s" % device
        except dbus.DBusException, e:
            pass
        if msg: 
            print msg
            self.show_current_devices_stack()
        

    def hardware_removed_interrupt(self, device):
        if  device in self.devices:
            i = self.devices.index(device)
            dev = self.devices.pop(i)
            print dev._i_am
            self.hardware_modify_notification(False, device=device, type=dev._i_am)
            print "= Device removed...OK -> %s" % device
            self.show_current_devices_stack()
        else:
            pass
            #print "= Device removed...IGNORED -> %s" % device
        

if __name__ == '__main__':
   hm = LampHardwareManager()
   def callback(data):
       print "+++++ Notify: %s" % data
   hm.set_hardware_insertion_notify_callback(callback)
   mainloop = gobject.MainLoop()
   mainloop.run()


