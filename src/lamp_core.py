#!/usr/bin/env python
#-*- coding: utf-8 -*-

#=========================
# Filename:  lamp_core.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-08-22
#--------------------------------------------------
# Description:
# It's responsible for the application management.
#--------------------------------------------------
# License:
#
#==================================================
from lamp_core_base import LampCoreBase
from lamp_utils.socket_server import SocketServer
#from lamp_utils.lamp_object_observer import ObjectObserver
import dbus
import dbus.service
import dbus.glib
import gobject
import sys

import time


class LampCore(dbus.service.Object):
    def __init__(self, bus_name, object_path="/Lampejo/Core"):
        dbus.service.Object.__init__(self, bus_name, object_path)
        self.home_socket_server = SocketServer()
        self.home_socket_server.start_server()
        self.lampejo_core_base = LampCoreBase()
        self.home_socket_server.polling_input_data(self.lampejo_core_base.decode_statement)
        #self.lampejo_core_base.set_error_callback(self.home_socket_server.write_channel)
        
        self.lampejo_core_base.hardware_manager.set_hardware_insertion_notify_callback(self.home_socket_server.write_channel)
        self.lampejo_core_base.hardware_manager.search_ready_devices()
        
    #def __del__(self):
    #    self.home_socket_server.shutdown_server()
    #    self.obj_obs.stop_observer()
    #    self.lampejo_core_base.shutdown()
    def turnoff(self):
        self.home_socket_server.shutdown_server()
        #self.obj_obs.stop_observer()
        self.lampejo_core_base.shutdown()
        
    @dbus.service.method("org.lampejo.CoreInteface")
    def show_home(self):
        self.lampejo_core_base.show_home()
        return "show home app!!!"
            
    @dbus.service.method("org.lampejo.CoreInteface")
    def switch_application(self):
        self.lampejo_core_base.switch_application()
        return "switch app!!!"
    
    @dbus.service.method("org.lampejo.CoreInteface")
    def close_application(self):
        self.lampejo_core_base.close_application()
        return "close app!!!"
    
    @dbus.service.method("org.lampejo.CoreInteface")
    def mount_pendrive(self):
        self.lampejo_core_base.mount_pendrive()
        return "mount pendrive!!!"
    
    @dbus.service.method("org.lampejo.CoreInteface")
    def unmount_pendrive(self):
        self.lampejo_core_base.unmount_pendrive()
        return "unmount pendrive!!!"
    
    @dbus.service.method("org.lampejo.CoreInteface")
    def shutdown_service(self):
        del self.lampejo_core_base
        raise KeyboardInterrupt()
        return "shutdown service!!!"
    
        
if __name__ == '__main__':
    try:
        if sys.argv[1] == "--hide":
            sys.stdout = open("/tmp/lamp-log","w+")
        else:
            print "Usage: lampejo_core [--hide]"
            raise SystemExit(1)
    except IndexError:
        pass
    session_bus = dbus.SessionBus()
    name = dbus.service.BusName("org.lampejo.CoreService", bus=session_bus)
    print """
    ======================================
    =      LAMPEJO CORE APPLICATION      = 
    --------------------------------------
    + Version: 0.1                       -  
    + Author: Rodrigo J. S. Peixoto      -
    + Contact email: rodrigopex at gmail -
    + License: GNU Public License        -
    --------------------------------------
    =      Lampejo Project Program       =
    ======================================
    """
    core = LampCore(name)
    try:
        print "Startup the Lampejo Core service daemon...ok"
        gobject.threads_init()
        dbus.glib.init_threads()
        main_loop = gobject.MainLoop()
        main_loop.run()
    except KeyboardInterrupt:
        core.turnoff()
        import subprocess
        print "Shutting down Lampejo Core service daemon...ok"
        sys.stdout.close()
        subprocess.call(["pkill", "python"])
    