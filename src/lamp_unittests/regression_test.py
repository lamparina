#!/usr/bin/env python
#-*- coding: utf-8 -*-
#=========================
# Filename:  regression_test.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-08-11
#--------------------------------------------------
# Description:
#
#--------------------------------------------------
# License:
#
#==================================================

import doctest
import os
import sys
import unittest

#append the src package to the path ------------
sys.path.append(os.path.join(os.getcwd(),".."))
#-----------------------------------------------
lamp_application_manager = __import__("lamp_application_manager")


suite = doctest.DocTestSuite(lamp_application_manager)
runner = unittest.TextTestRunner()
runner.run(suite)

