#!/usr/bin/env python
#-*- coding: utf-8 -*-
#=========================
# Filename:  application_manager_test.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-08-11
#--------------------------------------------------
# Description:
#
#--------------------------------------------------
# License:
#
#==================================================
import unittest
import os
import sys
import time
import subprocess

#append the src package to the path ------------
global LAMPEJO_CORE_PATH
LAMPEJO_CORE_PATH=os.getenv("LAMPEJO_CORE_PATH")
sys.path.append(LAMPEJO_CORE_PATH)
#-----------------------------------------------
lamp_application_manager=__import__("lamp_application_manager")


class TestCase(unittest.TestCase):

    def setUp(self):
        self.obj_test = None

    def test_constructor_empty(self):
        '''Testing the constructor with no args'''
        self.failUnlessRaises(TypeError, lamp_application_manager.LampApplication)

    def test_constructor_no_alias(self):
        '''Testing the constructor with "" alias name'''
        self.failUnlessRaises(AssertionError, lamp_application_manager.LampApplication, "")

    def test_constructor_all_arguments(self):
        '''Testing the constructor with corrects arguments'''
        obj_test = lamp_application_manager.LampApplication(u"lampejo",
                                                         exec_path=u"/opt/test",
                                                         exec_flags=u"-t -l -o rodrigo",
                                                         remote_control_image=u"/opt/lampejo/test",
                                                         replaceable=False)
        self.assert_(isinstance(obj_test, lamp_application_manager.LampApplication))

    def test_equals(self):
        '''Testing the equals built-in operation'''
        obj_test = lamp_application_manager.LampApplication(u"lampejo",
                                                         exec_path=u"/opt/test",
                                                         exec_flags=u"-t -l -o rodrigo",
                                                         remote_control_image=u"/opt/lampejo/test",
                                                         replaceable=False)
        obj_test2 = lamp_application_manager.LampApplication(u"lampejo",
                                                         exec_path=u"/mnt/test",
                                                         exec_flags=u"-test",
                                                         remote_control_image=u"/ccc/lamp/test",
                                                         replaceable=True)
        self.assert_(obj_test == obj_test2)
        obj_test2 = lamp_application_manager.LampApplication(u"lampejola",
                                                         exec_path=u"/mnt/test",
                                                         exec_flags=u"-test",
                                                         remote_control_image=u"/ccc/lamp/test",
                                                         replaceable=True)
        self.assert_(obj_test != obj_test2)

    def test_execute_itself_wrong_obj(self):
        '''Testing execute_itself with a wrong object'''
        self.obj_test = lamp_application_manager.LampApplication(u"lampejo",
                                                         exec_path=u"/opt/test",
                                                         exec_flags=u"-t -l -o rodrigo",
                                                         remote_control_image=u"/opt/lampejo/test",
                                                         replaceable=False)
        self.failUnlessRaises(OSError, self.obj_test.execute_itself)
    
    def test_execute_itself_wrong_obj2(self):
        '''Testing execute_itself with a wrong object arguments'''
        self.obj_test = lamp_application_manager.LampApplication(u"Error",
                                                         exec_path=1000,
                                                         exec_flags=u"-t -l -o rodrigo",
                                                         remote_control_image=u"/opt/lampejo/test",
                                                         replaceable=False)
        self.failUnlessRaises(AttributeError, self.obj_test.execute_itself)

    def test_execute_itself_good_obj(self):
        '''Testing execute_itself with a good object'''
        self.obj_test = lamp_application_manager.LampApplication(u"ls",
                                                         exec_path=u"/bin/ls",
                                                         exec_flags=u"-l -a -h",
                                                         remote_control_image=u"/opt/lampejo/test",
                                                         replaceable=True)
        self.obj_test.execute_itself()
        try:
            self.obj_test._pid // self.obj_test._pid
        except TypeError:
            self.fail("Pid was not generated, wrong execution")


    def test_execute_itself_good_obj2(self):
        '''Testing execute_itself with a simple good object and kill_itself'''
        self.obj_test = lamp_application_manager.LampApplication(u"env")
        self.obj_test.execute_itself()
        try:
            self.obj_test._pid // self.obj_test._pid
        except TypeError:
            self.fail( "Pid was not generated, wrong execution")
        self.obj_test.kill_itself()
        time.sleep(0.1)
        self.failIf(None == self.obj_test._process.poll())
    
    def test_execute_itself_good_obj2(self):
        '''Testing execute_itself with a simple good object HOME'''
        self.obj_test = lamp_application_manager.LampApplication(u"home",
                                                                 exec_path=u"/home/rodrigopex/Projects/lampejo/src/lamp_stubs/flash_stub.py",
                                                         window_title=u"home",
                                                         remote_control_image=u"/opt/lampejo/test",
                                                         file_source="",
                                                         replaceable=True)
        self.obj_test.execute_itself()
        #Test if the program runs ok
        #t = open("/tmp/lixo.dat","w+")
        #err = self.obj_test._process.stderr.read()
        #t.write(err)
        #self.assert_(""==err)
        try:
            self.obj_test._pid // self.obj_test._pid
        except TypeError:
            self.fail("Pid was not generated, wrong execution")
        self.obj_test.kill_itself()
        time.sleep(0.1)
        self.failIf(None == self.obj_test._process.poll())

    def test_execute_itself_opening_file(self):
        '''Testing execute_itself with a good object with good file path and kill_itself'''
        obj_test = lamp_application_manager.LampApplication(u"eog",
                                                         exec_path=u"/usr/bin/eog",
                                                         window_title=u"Eye of GNOME",
                                                         remote_control_image=u"/opt/lampejo/test",
                                                         file_source="../../media_test/test_picture.jpg",
                                                         replaceable=True)
        obj_test.execute_itself()
        try:
            obj_test._pid // obj_test._pid
        except TypeError:
            self.fail( "Pid was not generated, wrong execution")
        obj_test.kill_itself()
        time.sleep(0.1)
        self.failIf(None == obj_test._process.poll())
    
    
    def test_execute_itself_opening_file2(self):
        '''Testing execute_itself (ooffice) with a good object with good file path and kill_itself'''
        obj_test = lamp_application_manager.LampApplication(u"ooffice",
                                                         exec_path=u"/usr/bin/soffice",
                                                         window_title=u"OpenOffice.org",
                                                         remote_control_image=u"/opt/lampejo/test",
                                                         file_source="../../media_test/document.odt",
                                                         replaceable=True)
        obj_test.execute_itself()
        try:
            obj_test._pid // obj_test._pid
        except TypeError:
            self.fail("Pid was not generated, wrong execution")
        obj_test.kill_itself()
        time.sleep(0.1)
        self.failIf(None == obj_test._process.poll())

    
    def test_execute_itself_opening_file_wrong(self):
        '''Testing execute_itself with a good object with bad file path and kill_itself'''
        self.obj_test = lamp_application_manager.LampApplication(u"ooffice",
                                                         exec_path=u"/usr/bin/oofice",
                                                         window_title=u"OpenOffice.org",
                                                         remote_control_image=u"/opt/lampejo/test",
                                                         file_source="../../media_test/re",
                                                         replaceable=True)
        self.failUnlessRaises(OSError, self.obj_test.execute_itself)
       
    def test_kill_itself(self):
        '''Testing kill_itself with an invalid process'''
        obj_test = lamp_application_manager.LampApplication(u"lampejo",
                                                         exec_path=u"/opt/test",
                                                         exec_flags=u"-t -l -o rodrigo",
                                                         remote_control_image=u"/opt/lampejo/test",
                                                         replaceable=False)
        self.assert_(isinstance(obj_test, lamp_application_manager.LampApplication))
        self.failUnlessRaises(ValueError, obj_test.kill_itself)
        
    def test_kill_itself_wrong_pid(self):
        '''Testing kill_itself with an invalid process'''
        obj_test = lamp_application_manager.LampApplication(u"lampejo",
                                                         exec_path=u"/opt/test",
                                                         exec_flags=u"-t -l -o rodrigo",
                                                         remote_control_image=u"/opt/lampejo/test",
                                                         replaceable=False)
        obj_test._pid = 1000000000
        self.failUnlessRaises(OSError, obj_test.kill_itself)
    
    def test_is_replaceable_true(self):
        '''Testing is_replaceable with True value'''
        obj_test = lamp_application_manager.LampApplication(u"lampejo")
        self.failIf(obj_test.is_replaceable() == False,"The value must be True, got False")
        
    def test_is_replaceable_false(self):
        '''Testing is_replaceable with True value'''
        obj_test = lamp_application_manager.LampApplication(u"lampejo", replaceable=False)
        self.failIf(obj_test.is_replaceable(),"The value must be True, got False")

if __name__ == '__main__':
    unittest.main()

