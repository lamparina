#!/usr/bin/env python
#-*- coding: utf-8 -*-

#=========================
# Filename:  lamp_core_base.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-08-21
#--------------------------------------------------
# Description:
# It's responsible for the application management.
#--------------------------------------------------
# License:
#
#==================================================
from lamp_application_manager import LampApplicationManager
from lamp_application_manager import LampExecutionQueueIsEmptyError
from lamp_application_manager import LampExecutionQueueIsFullError
from lamp_hardware_manager import LampHardwareManager
from lamp_hardware_manager import VolumeIsNotMountedError
from lamp_hardware_manager import VolumeAlreadyMountedError
from lamp_bluetooth_manager import LampBluetoothManager

from lamp_utils.lamp_built_in_functions import check_types
from lamp_utils.lamp_built_in_functions import get_parent_path
from lamp_utils.lamp_recents_manager import LampRecentsManager
from lamp_utils.lamp_recents_manager import DirectoryNotFoundError
from lamp_utils.lamp_recents_manager import RecentsCannotBeCreatedError
import os
import re
import sys
import time
import subprocess


global LAMPEJO_CORE_PATH
LAMPEJO_CORE_PATH=os.getenv("LAMPEJO_CORE_PATH")
if LAMPEJO_CORE_PATH: 
    sys.path.append(LAMPEJO_CORE_PATH)
else:
    raise SystemExit("Error, the environment variable LAMPEJO_CORE_PATH is not defined.")

PATH_TO_CFG_FILE=os.path.join(get_parent_path(LAMPEJO_CORE_PATH), 'config', 'lampejo.cfg')
if not os.path.exists(PATH_TO_CFG_FILE):
    raise SystemExit("Error, the lampejo.cfg file is not found in the path: %s" % PATH_TO_CFG_FILE)
IMAGE_APP=u"eog"
AUDIO_VIDEO=u"kaffeine"
DOCUMENTS_PDF_PS=u"kpdf"
DUCOMENTS=u"soffice"

class InvalidStatementError(Exception): pass

class LampCoreBase:
    '''
    Tests:
    >>> test = LampCoreBase()
    '''
    def __init__(self):
        self.recents_manager = LampRecentsManager()
        self.app_manager = LampApplicationManager(PATH_TO_CFG_FILE)
        self.hardware_manager = LampHardwareManager()
        self.bluetooth_manager = LampBluetoothManager()
        self.bluetooth_manager.start_service()
        self.app_manager.set__update_control_image_callback(self.bluetooth_manager.change_control_image)
        self.__error_callback = None
        self.__hardware_insertion_notify_callback = None
        #INFO: Para utilizar no lampejo deve-se remover o comentário da linha abaixo
        #self.mouse_config()
    
    def mouse_config(self):
        print "Configure mouse buttons..."
        try:
            subprocess.call(["xmodmap", "-e","pointer = 1 116 115"])
        except:
            print "DEBUG: Error: mouse cannot be configured by core."
    
    def shutdown(self):
        self.app_manager.shutdown()
        self.bluetooth_manager.shutdown_service()
        #self.hardware_manager.s
        
        #self.recents_manager.s
        pass
    
    def decode_statement(self, data):
        '''
        Tests:
        >>> import time
        >>> test = LampCoreBase()
        >>> test.decode_statement("#exec firefox#")
        >>> test.decode_statement("#exec firefox##exec firefox#")
        Traceback (most recent call last):
            ...
        InvalidStatementError: The statement is no valid!
        >>> test.decode_statement("#exec firefox##exec firefox##exec fir")
        Traceback (most recent call last):
            ...
        InvalidStatementError: The statement is no valid!
        >>> test.decode_statement("#open ../media_test/presentation.odp#")
        >>> time.sleep(2)
        >>> test.switch_application()
        >>> time.sleep(2)        
        >>> test.switch_application()
        >>> time.sleep(2)
        >>> test.switch_application()
        >>> time.sleep(2)        
        >>> test.switch_application()
        >>> test.close_application()
        >>> test.close_application()
        '''
        print "DEBUG: from flash -> ", data
        if data == "#started#":
            for dev in self.hardware_manager.devices:
                time.sleep(0.5)
                self.hardware_manager.hardware_modify_notification(added=True, device=dev)
        else:
            #FUnciona => re.match(r"^#(exec|open) ([a-zA-Z0-9-_.]+|[a-zA-Z0-9. _/-]+)#$", data) and \
            data = data and \
               re.match(r"^#(exec|open|save|delete|close) .*#$", data) and \
               data[1:-1] or None
            if data:
                #test = re.compile(r"^((?P<e>exec)|(?P<o>open)) (?(e)[a-zA-Z0-9-_ .]+)(?(o)[a-zA-Z0-9. _/-]+)$")
                #command_content = data and test.match(data) and re.sub("(exec |open )", "", data) or None
                command_content = re.sub("(exec |open |save |delete |close )", "", data) or None
                do_action = {"exec": self.execute_application, 
                             "open": self.open_file,
                             "save": self.save_dir,
                             "delete":self.delete_dir,
                             "close":self.close_window_by_flash}
                action =  data[:data.index(" ")]
                if type(command_content) == type(u""):
                    do_action[action](command_content)
                else:
                    do_action[action](u'"%s"' % unicode(command_content, "UTF-8"))
            else:
                print "DEBUG: Warning the statement (%s) is not valid!" % data
                #raise InvalidStatementError("The statement is no valid!")
            
    
    def execute_application(self, command_content):
        try:
            command_content and self.app_manager.run_application(command_content)
        except LampExecutionQueueIsFullError, e:
            self.show_error(e)
    
    def open_file(self, command_content):
        try:
            app_alias = command_content and self.check_extension(command_content)
            command_content and app_alias and self.app_manager.run_application(app_alias, command_content)
        except LampExecutionQueueIsFullError, e:
            self.show_error(e)
            
    def save_dir(self, command_content):
        pendrive = self.hardware_manager.get_storage_device()
        if pendrive:
            mp = pendrive.mount_point()
            print "Save dir: %s to %s!" % (command_content, mp)
            #TODO: Basta descomentar que está pronto!
            self.recents_manager.save_dir(command_content, mp)
        else:
            print "DEBUG: Saving error -> pendrive not found!"
    
    def delete_dir(self, command_content):
        print "Delete dir: %s!" % command_content
        #TODO: Basta descomentar que está pronto!
        self.recents_manager.remove_contents_of_dir(command_content)
    
    def close_window_by_flash(self, command_content):
        print "Close window: %s" % command_content
        #TODO: Basta descomentar que está pronto!
        self.close_application()
   
    def set_error_callback(self, callback):
        if callback and check_types({callback: type(lambda:None)}):
            self.__error_callback = callback
    
    def show_error(self, message):
        '''This is temporary!!! It depends the zenity application.
        Tests:
        >>> test = LampCoreBase()
        >>> def tmp(msg, call): print msg
        >>> test.show_error("Test1")
        >>> test.show_error("Test2")
        >>> test.show_error("Test3")
        '''
        if self.__error_callback:
            self.__error_callback(message)
        else:
            print "DEBUG: Error -> __error_callback not valid!"
            print "DEBUG: message -> %s" % message
        
    def check_extension(self, file):
        '''
        Tests:
        >>> test = LampCoreBase()
        >>> test.check_extension("/home/lamp/test/image.png")
        u'eog'
        >>> test.check_extension("/home/lamp/test/music.ogg")
        u'totem'
        >>> test.check_extension("/home/lamp/test/document.odt")
        u'ooffice'
        >>> test.check_extension("/home/lamp/test/document.pdf")
        u'evince'
        >>> test.check_extension("/home/lamp/test/document.pddsss")
        '''
        ret = None
        apps = {DOCUMENTS_PDF_PS: [".pdf", ".ps"], 
                DUCOMENTS: [".odt", ".ods", ".odp", ".doc", ".xls", ".ppt", ".pps",".docx", ".xlsx", ".pptx"], 
                IMAGE_APP: [".jpg", ".png", ".bmp", ".gif"], 
                AUDIO_VIDEO: [".mp3",".ogg", ".dvd", ".avi", ".mpeg",".mpg", ".mov", ".wmv",".wav", ".wma"],
                u"firefox": ["swf", "gif"]}
        file_name, file_ext = os.path.splitext(file)
        for app, exts in apps.items():
            if file_ext in exts:
                ret = app
                break
        if not ret:
            print "DEBUG: Warning extension %s not supported yet!" % file_ext
        return ret
    
    def show_home(self):
        self.app_manager.show_home()
    
    def switch_application(self):
        self.app_manager.switch_application()
        
    def close_application(self):
        try:
            self.app_manager.kill_current_application()
        except LampExecutionQueueIsEmptyError:
            message = "Info: Trying to close an application; There is not application running now."
            self.show_error(message)

    def mount_pendrive(self):
        '''
        
        Tests:
        >>> test = LampCoreBase()
        >>> test.mount_pendrive()
        '''
        for dev in self.hardware_manager.devices:
            try:
                if dev.who_am_i() == "storage":
                    return dev.mount()
                else:
                    self.show_error("Warning: There is not a pendrive!")
            except VolumeAlreadyMountedError:
                self.show_error("Warning: The pendrive is already mounted!")

            
    def unmount_pendrive(self):
        '''
        
        Tests:
        >>> test = LampCoreBase()
        >>> test.unmount_pendrive()
        '''
        for dev in self.hardware_manager.devices:
            try:
                if dev.who_am_i() == "storage":
                    return dev.eject()
                    #self.hardware_manager.hardware_modify_notification(added=False, device=dev)
                else:
                    self.show_error("Warning: There is not a pendrive!")
            except VolumeIsNotMountedError:
                self.show_error("Warning: The pendrive is already unmounted!")


def _test():
    import doctest
    doctest.testmod()
    

if __name__ == '__main__':
    _test()
