#!/usr/bin/env python
#-*- coding: utf-8 -*-
#=========================
# Filename:  lamp_recents_manager.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-09-27
#----------------------------------------------
# Description:
# It's responsible for the recents management.
#----------------------------------------------
# License:
#
#==============================================

import shutil
import os.path
from shell_rip import Bash
from shell_rip import ShellError

class DirectoryNotFoundError(Exception): pass
class RecentsCannotBeCreatedError(Exception): pass

class LampRecentsManager:
    def __init__(self):
        self.sh = Bash()
        self.create_recents_folder()
            
    def __del__(self):
        try:
            self.sh.rm("-rf","/tmp/Recentes")
        except ShellError, e:
            print e
    
    def create_folder(self, folder):
        try:
            self.sh.mkdir(folder)
        except ShellError, e:
            print "DEBUG: Folder creation error:",e
    
    def create_recents_folder(self):
        self.create_folder("/tmp/Recentes")
        self.create_folder("/tmp/Recentes/fotos")
        self.create_folder("/tmp/Recentes/gravações")
        self.create_folder("/tmp/Recentes/filmes")
        self.create_folder("/tmp/Recentes/downloads")
        
    def save_dir(self, src, dst="/media/pendrive"):
        try:
            self.sh.cp("-r", src, dst)
        except ShellError, e:
            print "DEBUG: saving error:", e
    
    def remove_contents_of_dir(self, dir):
        if os.path.exists(dir):
            self.sh.rm("-rf", dir)
            if "Recentes" in dir:
                self.create_recents_folder()
            else:
                self.sh.mkdir(dir)
        else:
            raise DirectoryNotFoundError(dir)
    
if __name__ == '__main__':
    print "Criando recentes...",
    test = LampRecentsManager()
    print "ok"
    raw_input("Delete Recentes?")
    del test