import lightblue
s = lightblue.socket()
s.bind(("", 0))  # bind to 0 to bind to dynamically assigned port 
s.listen(1)
lightblue.advertise("My RFCOMM Service", s, lightblue.RFCOMM)
conn, addr = s.accept()
print "Connected by", addr
conn.recv(1024)
conn.close()
s.close()
