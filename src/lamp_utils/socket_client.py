#!/usr/bin/env python
#-*- coding: utf-8 -*-

#=========================
# Filename:  client_socket.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-08-20
#--------------------------------------------------
# Description:
# It's responsible for the application management.
#--------------------------------------------------
# License:
#
#==================================================

import socket
import time
import sys
import subprocess
from threading import Thread
from threading import Event
 
HOST = 'localhost'    # The remote host
PORT = 50007 # The same port as used by the server


def threaded(func):
    def proxy(*args, **kwargs):
        thr = Thread(target=func, args=args, kwargs=kwargs)
        thr.setDaemon(True)
        thr.start()
        return thr
    return proxy

class DataCannotBeSentError(Exception): pass

class SocketConnectionError(Exception): pass

class SocketClient:
    def __init__(self):
        self.skt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__stop_signal = Event()
        self.__stop_signal.set()
        try:
            self.skt.connect((HOST, PORT))
        except socket.error:
            raise SocketConnectionError("The connection cannot be established. Verify the server.")
        
    def read_channel(self):
        data = None
        try:
            data = unicode(self.skt.recv(64), "UTF-8").strip()
            print "DEBUG: Socket Client reading %s" % data
        except socket.error:
            raise 
        return data
        
    def write_channel(self, data):
        try:
            self.skt.send(data)
        except socket.error:
            raise DataCannotBeSentError("Socket Error, the data can not be sent.")
    
    @threaded
    def polling_input_data(self, callback):
        assert type(callback) and type(lambda:0), "Error: callback must be a function!"
        while self.__stop_signal.isSet():
            time.sleep(0.5)
            data = self.read_channel()
            data and callback(data)
        print "polling is out!"
