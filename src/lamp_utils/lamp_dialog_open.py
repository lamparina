#!/usr/bin/env python
#-*- coding: utf-8 -*-
#=========================
# Filename:  lamp_dialog_open.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-10-07
#-------------------------
# Description:
#
#-------------------------
# License:
#
#=========================
import gtk
import gtk.glade
import time
from threading import Thread
from threading import Event
import sys
import os

def threaded(func):
    def proxy(*args,**kwargs):
        thr = Thread(target=func, args=args, kwargs=kwargs)
        thr.start()
        return thr
    return proxy

class LampOpenDialog:
    def __init__(self):
        path_glade = os.path.join(os.environ["LAMPEJO_CORE_PATH"],"lamp_utils","lamp_dialog_open.glade")
        print path_glade
        gladefile = gtk.glade.XML(path_glade)
        self.dialog = gladefile.get_widget("dialog_open")
        self.progressbar = gladefile.get_widget("progressbar")
        self.dialog.connect("destroy",gtk.main_quit)
        self.__run_pulse = Event()
        self.__run_pulse.set()
        
    @threaded
    def pulse_progressbar(self, timeout=0):
        if timeout:
            self.dialog.show()
            timeout = timeout*10
            while timeout and self.__run_pulse.isSet():
                time.sleep(0.1)
                gtk.gdk.threads_enter()
                self.progressbar.pulse()
                gtk.gdk.threads_leave()
                timeout-=1
            self.destroy_opening_dialog()
    
    """@threaded
    def run(self, timeout):
        self.dialog.show()
        self.pulse_progressbar()
        if timeout:
            self.count_time(timeout)
           

    @threaded
    def fade_out(self):
        op = 0.6
        while op > 0.1:
            time.sleep(0.03)
            op = self.dialog.get_opacity()
            self.dialog.set_opacity(op-0.05)
        self.destroy_opening_dialog()

    @threaded
    def count_time(self, t):
        time.sleep(t)
        self.fade_out()
   """ 
    def destroy_opening_dialog(self, *args):
        self.__run_pulse.clear()
        self.dialog.destroy()
        gtk.main_quit()

if __name__ == '__main__':
    test = timeout = None
    try:
        timeout = int(sys.argv[1])
        assert type(timeout) == type(1)
    except:
        raise SystemExit(1)
    else:
        test = LampOpenDialog()
        test.pulse_progressbar(timeout)
        gtk.gdk.threads_init()
        gtk.gdk.threads_enter()
        gtk.main()
        gtk.gdk.threads_leave()