#!/usr/bin/env python
#-*- coding: utf-8 -*-

#=========================
# Filename:  server_socket.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-08-20
#--------------------------------------------------
# Description:
# It's responsible for the application management.
#--------------------------------------------------
# License:
#
#==================================================

from threading import Thread

def threaded(func):
    def proxy(*args, **kwargs):
        thr = Thread(target=func, args=args, kwargs=kwargs)
        thr.start()
        return thr
    return proxy
