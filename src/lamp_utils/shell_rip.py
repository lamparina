#!/usr/bin/env python
#-*- coding: utf-8 -*-
#=========================
# Filename:  shell_rip.py
# Author: 
# Date: 2008-09-27
#----------------------------------------------
# Description:
# It's responsible for the recents management.
#----------------------------------------------
# License:
#
#==============================================

import subprocess

class ShellError(Exception): pass

class Bash(object):
    def __getattribute__(self, cmd):
        def proxy(*args):
            command_with_args = [cmd] + list(args)
            p = subprocess.Popen(command_with_args,
                                 stdout=subprocess.PIPE,
                                 stderr=subprocess.PIPE)
            error = p.stderr and p.stderr.read()
            if error:
                raise ShellError(error)
            else:
                return p.stdout and p.stdout.read()
            
        return proxy
    
if __name__ == "__main__":
    sh = Bash()
    try:
        print sh.rm("-rf", "/tmp/Recentes")
    except ShellError, e:
        print e