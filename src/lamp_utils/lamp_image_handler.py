#!/usr/bin/env python
#-*- coding: utf-8 -*-
#=========================
# Filename:  lamp_iamge_handle.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-09-30
#--------------------------------------------------
# Description:
# It's responsible for 
#--------------------------------------------------
# License:
#
#==================================================

#!/usr/bin/env python

import cairo

WIDTH, HEIGHT = 320, 240

# Setup Cairo
def show_text(ctx, text):
    ctx.save()
    ctx.set_font_size (15)
    ctx.set_source_rgba(1, 1, 1, 0.8)
    ctx.move_to(320 - (len(text)*15), 210)
    ctx.show_text (text)
    ctx.stroke()
    ctx.restore()

def save(surface, file_name):
    surface.write_to_png(file_name)

def random_picture():
    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, WIDTH, HEIGHT)
    ctx = cairo.Context(surface)
    ctx.save()
    import random
    for i in range(320):
        for k in range(240):
            #if 40 < k < 200:
                ctx.set_source_rgb(random.random(), random.random(), random.random())
                ctx.rectangle (i, k, 1, 1)
                ctx.stroke()
    
    ctx.select_font_face ("Purisa", 
                            cairo.FONT_SLANT_NORMAL, 
                            cairo.FONT_WEIGHT_BOLD)
    show_text(ctx, text="Lampejo - photo2!")
    save(surface, "/tmp/triangle.png")
    ctx.restore()

def build_picture(width, height, image, file_name):
    surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, width, height)
    ctx = cairo.Context(surface)
    ctx.save()
    count = 0
    for i in range(width):
        for k in range(height):
            ctx.set_source_rgb(*image[count])
            ctx.rectangle (i, k, 1, 1)
            ctx.stroke()
            count+=1

    ctx.select_font_face ("Purisa", 
                            cairo.FONT_SLANT_NORMAL, 
                            cairo.FONT_WEIGHT_BOLD)
    show_text(ctx, text="Lampejo - photo2!")
    save(surface, file_name)
    ctx.restore()


def translate_from_rgb3bytes(image):
    ret = None
    tmp = []
    length = len(image)
    ret = []
    for byte in range(0, length, 3):
        try:
            ret.append((ord(image[byte])/255.0, 
                        ord(image[byte+1])/255.0, 
                        ord(image[byte+2])/255.0))
            tmp.extend([hex(ord(image[byte])), 
                        hex(ord(image[byte+1])), 
                        hex(ord(image[byte+2]))])
        except IndexError:
            ret = None
            print "Picture malformed!"
            break
    tmpf = open("/tmp/out_img.hex","w+")
    print len(tmp)
    for i in xrange(len(tmp),1000):
        tmp.insert(i,"\n")
    tmp = ",".join(tmp)
    #print tmp
    tmpf.write("char image[] = {%s};" % tmp)
    tmpf.close()
    return ret

def gen_py_image(file_src,file_dest):
    raw_image = get_file_c(file_src)
    f = open(file_dest,"w+")
    f.write('im="%s"' % raw_image)
    f.close()

def get_file_c(file_name):
    f = open(file_name,"r")
    lines = f.readlines()
    ret = []
    for l in lines:
        ret.append(l[1:-2])#.strip()[1:-2])
    return "".join(ret)
        

if __name__ == '__main__':
    #random_picture()
    im =  "\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"\
          "\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"\
          "\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"\
          "\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"\
          "\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377"\
          "\377\377\377\377\377\377\377\377\377\377\377\377\377\377\377\0\0\0\0\0\0"\
          "\0\0\0\0\0\0\0\0\0\377\377\377\377\377\377\377\377\377\377\377\377\0\0\0"\
          "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\377\377\377\377\377\377\377\377\377\0\0\0"\
          "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\377\377\377\377\377\377\377\377\377"\
          "\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\377\377\377\377\377\377\377\377"\
          "\377\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\377\377\377\377\377\377\377"\
          "\377\377\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\377\377\377\377\377\377"\
          "\377\377\377\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0"
          
    #image = translate_from_rgb3bytes(im)
    #print image
    #print len(image)
    #build_picture(10, 10, image, "/tmp/pequena.png")
    
    
    gen_py_image("/home/rodrigopex/Desktop/velha_mini.c", "image_tmp.py")
    import image_tmp
    raw_image = image_tmp.im
    print len(raw_image)
    image = translate_from_rgb3bytes(raw_image)
    print len(image)
    build_picture(10, 10, image, "/tmp/velha_mini.png")
