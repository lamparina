import dbus
import time
import sys

bus = dbus.SystemBus()
bmgr = dbus.Interface(bus.get_object('org.bluez', '/org/bluez'), 'org.bluez.Manager')
bus_id = bmgr.ActivateService('serial')
serial = dbus.Interface(bus.get_object(bus_id, '/org/bluez/serial'), 'org.bluez.serial.Manager')
# Service connection, read the serial API to check the available patterns
if (len(sys.argv) < 3):
        print "Usage: %s <adapter> <address> [service]" % (sys.argv[0])
        sys.exit(1)
adapter = sys.argv[1]
address = sys.argv[2]
if (len(sys.argv) < 4):
        service = "spp"
else:
        service = sys.argv[3]

device = serial.ConnectServiceFromAdapter(adapter, address, service)
print "Connected %s to %s" % (device, address)
print "Press CTRL-C to disconnect"
try:
        time.sleep(1000)
        print "Terminating connection"
except:
        pass

serial.DisconnectService(device)