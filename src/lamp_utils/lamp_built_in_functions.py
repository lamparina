#!/usr/bin/env python
#-*- coding: utf-8 -*-
#=========================
# Filename:  lamp_built_in_functions.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-08-11
#---------------------------------------
# Description:
# This module has the useful functions
#---------------------------------------
# License:
#
#==================================================

import os

class LampArgumentTypeError(ValueError): pass

def check_types(check_dict=None):
    '''verify_args(check_dict) -> bool

    It verifies the types of keyval with the value types of the check_dict dict.

    Arguments:

        check_dict -- dict with the values which will be checked at the dict key
        and the types that will checks the values at the dict values
        Ex.:
            # to checks v1 and v2 as string and fload we must do:
            python> check_types(**{v1: str, v2: float}) 

    Return:

        - True if the values types are ok
        - False if the values types are wrong

    Raise:
        - AssertionError "Argument check_dict is not a dict" if the check_dict
        arguments is not dict

    Tests:

    >>> check_types({1: int, 2: str, "": int, 3.14: float})# [1,2,"",3.14],[int, str, int, float])
    False
    >>> check_types({"Firefox": str})
    True
    >>> check_types({1: int, 2: int, "": str, 3.14: float})#[1,2,"",3.14],[int, int, str, float])
    True
    >>> check_types({3.14: 0.1}) #[3.14],[float])
    Traceback (most recent call last):
        ...
    LampArgumentTypeError: Verify the check_dict values, it must be a class, type, or tuple of classes and types
    >>> check_types([3.14])
    Traceback (most recent call last):
        ...
    AssertionError: Argument check_dict is not a dict
    >>> check_types((1,2,3,3.14))
    Traceback (most recent call last):
        ...
    AssertionError: Argument check_dict is not a dict
    '''
    ret = False
    try:
        assert isinstance(check_dict, dict), "Argument check_dict is not a dict"
        ret = all(map(isinstance,check_dict.keys(), check_dict.values()))
    except TypeError:
        raise LampArgumentTypeError("Verify the check_dict values, it must be a class, type, or tuple of classes and types")
    return ret

def get_parent_path(path):
    '''
    
    Tests:
    >>> get_parent_path("/home/test/now")
    '/home/test'
    >>> get_parent_path(get_parent_path("/home/test/now"))
    '/home'
    >>> get_parent_path(get_parent_path(get_parent_path("/home/test/now")))
    '/'
    '''
    
    check_types({path: str})
    if path[-1] == os.path.sep:
        path = path[:-1]
    return os.path.split(path)[0]

def _test():
    import doctest
    doctest.testmod()

if __name__ == "__main__":
    _test()

