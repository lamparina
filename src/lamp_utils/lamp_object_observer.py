#!/usr/bin/env python
#-*- coding: utf-8 -*-

#=========================
# Filename:  lamp_object_observer.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-08-29
#--------------------------------------------------
# Description:
#
#--------------------------------------------------
# License:
#
#==================================================
from easy_thread import threaded
from threading import Event
import copy
import time
import sys

class ObjectObserver:
    def __init__(self, obj, callback):
        #try:
        #    assert isinstance(callback, type(lambda:None)), "Error: callback invalid!"
        #except AssertionError:
        #    import pdb
        #    pdb.post_mortem(sys.exc_info()[2])
            
        self.__call_back = callback
        self.__stop_observer = Event()
        self.__stop_observer.set()
        self._object = obj
        
    def stop_observer(self):
        self.__stop_observer.clear()
    
    @threaded
    def start_observer(self):
        print dir(self._object)
        dcp = copy.deepcopy(self._object)
        while self.__stop_observer.isSet():
            if self._object != dcp:
                time.sleep(0.6)
                try:
                    self.__call_back()
                except:
                    import pdb
                    pdb.post_mortem(sys.exc_info()[2])
                dcp = copy.deepcopy(self._object)
            time.sleep(1)
            
if __name__ == "__main__":
    class Item:
        def __init__(self, val):
            self.my_value = val
            
    class TestClass:
        def __init__(self):
            self.objs = []
        def __repr__(self):
            return str(self.objs)
    
    test = TestClass()
    
    def print_current_state(data):
        f = open("/tmp/lixo.dat", "a+")
        f.write(str(data))
        f.close()
    
    obx = ObjectObserver(test.objs, print_current_state)
    obx.start_observer()
    
    for i in xrange(5):
        test.objs.append(Item(raw_input("digite o valor: ")))

    print "End of ObjectObserver test..."
    obx.stop_observer()