#!/usr/bin/env python
#-*- coding: utf-8 -*-

#=========================
# Filename:  server_socket_rfcomm.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-09-16
#--------------------------------------------------
# Description:
# It's responsible for the rfcomm socket management.
#--------------------------------------------------
# License:
#
#==================================================

from threading import Event
from threading import Thread
from bluetooth import *
import sys
import time

HOST = '11:11:11:11:11:11' # Symbolic name meaning the local host
CHANNEL = 10               # Arbitrary non-privileged port

def threaded(func):
    def proxy(*args, **kwargs):
        thr = Thread(target=func,args=args, kwargs=kwargs)
        thr.start()
        return thr
    return proxy

class SocketServerRfcomm:
    def __init__(self, host=HOST, channel=CHANNEL):
        self.sckt = BluetoothSocket(RFCOMM)
        self.sckt.settimeout(0.5)
        self.sckt.bind((host, channel))
        self.sckt.listen(1)
        self.__stop_signal = Event()
        self.__polling_callback = None
        self.__running = True
        self.remote_control = None
    
    def set_polling_callback(self, callback):
        assert type(callback) and type(lambda:0), "Error: callback must be a function!"
        self.__polling_callback = callback
    
    @threaded
    def start_server(self):
        print "Start Bluetooth connection server..."
        while self.__running:
            try:
                conn, addr = self.sckt.accept()
                self.remote_control = conn
                print "ok"
                #print dir(self.remote_control)
                print 'DEBUG: Connected to Socket Client Rfcomm by %s' % str(addr)
                self.polling_input_data()
                break
            except:
                pass
        else:
            print "fail"
    
    def restart_server(self):
        print "Trying to restart the bluetooth connection server..."
        self.__stop_signal.clear()
        self.remote_control.shutdown(2)
        self.remote_control = None
        self.start_server()


    def timeout_receiver(self, n):
        ret = None
        tmp = []
        for i in xrange(n):
            while self.remote_control and self.__running:
                try:
                    tmp.append(self.remote_control.recv(1))
                    break
                except BluetoothError, e:
                    print "error receiver:", e
                    #print len(e.message),e.message[1:4]
                    #if e.message[1:4] == "104":
                    #    print "Restarting bluetooth server..."
                    self.restart_server()
                    
                        
        data = "".join(tmp)
        return data
    
    def timeout_sender(self, data):
        tmp = []
        while self.__running:
            try:
                self.remote_control and self.remote_control.sendall(data)
                break
            except BluetoothError, e:
                print "error sender:", e
                #if e.message[1:4] == "107":
                #        print "Restarting bluetooth server..."
                self.restart_server()

    def read(self, n=5):
        data = None
        try:
            data = self.timeout_receiver(n)
        except:
            print "DEBUG: Error socke read: ", sys.exc_info()
        return data
    
    def write(self, data):
        #data = "0123" + data
        msg = "DEBUG: Socket Server sending %s" % data
        print msg
        try:
            self.timeout_sender(data)
        except:
            print "DEBUG: Error socke write: ", sys.exc_info()
        
    @threaded
    def polling_input_data(self):
        print "DEBUG: start polling!"
        self.__stop_signal.set()
        while self.__stop_signal.isSet():
            time.sleep(0.5)
            data = self.read()
            data and self.__polling_callback and self.__polling_callback(data)
        print "DEBUG: polling is out!"

    def stop_polling_input_data(self):
        #self.__running = False
        #self.__stop_signal.clear()
        time.sleep(0.5)
        #self.__running = True
    
    def restart_polling_input_data(self):
        self.polling_input_data()
                
    def shutdown_server(self):
        self.__running = False
        self.__stop_signal.clear()
        self.remote_control and self.remote_control.close()
        time.sleep(1)
        self.sckt.close()
        print "DEBUG: RFCOMM server shutting down...ok"
        sys.exit(0)
    
    
if __name__=="__main__":
    srv = SocketServerRfcomm()
    srv.start_server()
    def tmp(p):
        print p
    srv.set_polling_callback(tmp)
    srv.polling_input_data()
    raw_input("Shutdown server now? [Enter]")
    srv.shutdown_server()
