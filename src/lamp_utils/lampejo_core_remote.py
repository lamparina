#!/usr/bin/env python

from optparse import OptionParser
import dbus
import sys

global CORE_INTERFACE
CORE_INTERFACE=None

# DBus init connection ========================================================
try:
	bus = dbus.SessionBus()
	remote_object = bus.get_object("org.lampejo.CoreService", "/Lampejo/Core")
	remote_object.Introspect(dbus_interface="org.freedesktop.DBus.Introspectable")
	CORE_INTERFACE = dbus.Interface(remote_object, "org.lampejo.CoreInteface")
except dbus.exceptions.DBusException:
	pass
#==============================================================================

def run_command(lamp_core_interface=CORE_INTERFACE):
	if CORE_INTERFACE:
		parser = OptionParser(usage="%prog [option]", version="%prog 0.1")
		
		parser.add_option("-i",
						  "--show_home",
						  action="store_true",
						  dest="home",
						  default=False,
						  help="Show the home application")
		parser.add_option("-s",
						  "--switch_app",
						  action="store_true",
						  dest="sw_app",
						  default=False,
						  help="Switch application")
		
		parser.add_option("-c",
						  "--close_current_app",
						  action="store_true",
						  dest="cl_app",
						  default=False,
						  help="Close current on focus application")
		
		parser.add_option("-m",
						  "--mount_pendrive",
						  action="store_true",
						  dest="mn_pen",
						  default=False,
						  help="Mount the pendrive")
		
		parser.add_option("-u",
						  "--unmount_pendrive",
						  action="store_true",
						  dest="unm_pen",
						  default=False,
						  help="Unmount the pendrive")
		
		(options, args) = parser.parse_args()
		
		#print options
		if options.home:
			lamp_core_interface.show_home()
		elif options.cl_app:
			lamp_core_interface.close_application()
		elif options.sw_app:
			lamp_core_interface.switch_application()
		elif options.mn_pen:
			lamp_core_interface.mount_pendrive()
		elif options.unm_pen:
			lamp_core_interface.unmount_pendrive()
		else:
			parser.print_help()
	else:
		sys.stderr.write("Error: The CoreService daemon cannot be found. Run it and try again.\n")

if __name__ == '__main__':
	run_command()