#!/usr/bin/env python
#-*- coding: utf-8 -*-

#=========================
# Filename:  server_socket.py
# Author: Rodrigo J. S. Peixoto
# Date: 2008-08-20
#--------------------------------------------------
# Description:
# It's responsible for the application management.
#--------------------------------------------------
# License:
#
#==================================================

from easy_thread import threaded
from threading import Event
import socket
import sys
import time

HOST = '127.0.0.1'                 # Symbolic name meaning the local host
PORT = 50000              # Arbitrary non-privileged port

class SocketServer:
    def __init__(self):
        self.sckt = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sckt.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sckt.bind((HOST, PORT))
        self.sckt.listen(5)
        self.conns = []
        self.__stop_signal = Event()
        self.__stop_signal.set()
        self.log = open("/tmp/server_socket.log", "w+")


    @threaded
    def start_server(self):
        conn, addr = self.sckt.accept()
        self.conns.append((conn, addr))
        print 'Connected to Socket Server by %s' % str(addr)
        
    def read_channel(self):
        data = None
        for con in self.conns:
            try:
                data = unicode(con[0].recv(512), "UTF-8").strip()
                break
            except socket.error:
                pass
            
        return data
    
    def write_channel(self, data):
        msg = "DEBUG: Socket Server sending %s" % data
        print msg
        self.log.write(msg)
        for c in self.conns:
            try:
                c[0].sendall(data)
            except socket.error:
                pass
        
    @threaded
    def polling_input_data(self, callback):
        assert type(callback) and type(lambda:0), "Error: callback must be a function!"
        while self.__stop_signal.isSet():
            time.sleep(0.5)
            data = self.read_channel()
            data and callback(data)
        print "polling is out!"
                
    @threaded
    def shutdown_server(self):
        self.__stop_signal.clear()
        for con in self.conns:
            con[0].close()
        time.sleep(1)
        self.sckt.close()
        sys.exit(0)
    
    
if __name__=="__main__":
    srv = SocketServer()
    srv.start_server()
    def tmp(p):
        srv.write_channel(p)
    srv.polling_input_data(tmp)
    raw_input("Shutdown server now? [Enter]")
    srv.shutdown_server()
